-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2017 at 07:43 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alumni5`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_kerja`
--

CREATE TABLE `tb_kerja` (
  `id_kerja` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `nim` varchar(9) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `website_perusahaan` varchar(100) NOT NULL,
  `instansi_tempat_kerja` varchar(100) NOT NULL,
  `tahun_masuk_kerja` char(4) NOT NULL,
  `posisi_jabatan_saat_ini` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kerja`
--

INSERT INTO `tb_kerja` (`id_kerja`, `id`, `nim`, `nama_perusahaan`, `website_perusahaan`, `instansi_tempat_kerja`, `tahun_masuk_kerja`, `posisi_jabatan_saat_ini`, `updated_at`) VALUES
(1, 4, '216720829', 'PT Mitra Data Abadi', 'http://mitradataabadi.com', 'Perseroan', '2016', 'Entry Data', '2017-11-02 19:34:38');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kontak`
--

CREATE TABLE `tb_kontak` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `judul` varchar(128) NOT NULL,
  `isi` text NOT NULL,
  `is_dibalas` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=belum dibalas; 1=sudah dibalas',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_kontak`
--

INSERT INTO `tb_kontak` (`id`, `nama`, `email`, `judul`, `isi`, `is_dibalas`, `created_at`, `updated_at`) VALUES
(1, 'Muhammad Abdul Aziz', 'abdul.aziz@stmik-aub.ac.id', 'Tidak Bisa Login', 'Saya tidak bisa login.', '0', '2017-10-02 20:35:17', '2017-10-02 20:35:17'),
(2, 'Muhammad Naufal Maarif', 'naufal.maarif@stmik-aub.ac.id', 'Tidak Bisa Login', 'Saya tidak bisa login, username dan password yang diberikan saya lupa.', '0', '2017-10-03 17:57:24', '2017-10-03 17:57:24');

-- --------------------------------------------------------

--
-- Table structure for table `tb_mahasiswa`
--

CREATE TABLE `tb_mahasiswa` (
  `id` int(10) NOT NULL,
  `email` varchar(64) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` char(8) NOT NULL,
  `nim` varchar(9) NOT NULL,
  `nama` varchar(64) NOT NULL,
  `nama_panggilan` varchar(32) NOT NULL,
  `jenis_kelamin` enum('P','L') DEFAULT NULL,
  `agama` enum('0','1','2','3','4','5','6') DEFAULT NULL COMMENT '0=lainnya; 1=islam; 2=katolik; 3=protestan; 4=hindu; 5=budha; 6=Konghucu',
  `ket_agama` varchar(32) DEFAULT NULL,
  `tempat_lahir` varchar(32) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `berat_badan` varchar(3) DEFAULT NULL,
  `tinggi_badan` varchar(3) DEFAULT NULL,
  `tmp_alamat` varchar(255) DEFAULT NULL,
  `tmp_telepon` varchar(16) DEFAULT NULL,
  `ort_nama_ayah` varchar(64) DEFAULT NULL,
  `status_pendaftaran` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=mengundurkan diri; 1=aktif',
  `status_biodata` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=belum lengkap; 1=sudah lengkap',
  `status_verifikasi` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=belum; 1=sudah',
  `status_seleksi` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 = tidak lulus; 1=lulus;',
  `status_perkawinan` enum('S','B') DEFAULT NULL,
  `fakultas` varchar(35) NOT NULL,
  `jur_prodi` varchar(100) NOT NULL,
  `tanggal_lulus` date NOT NULL,
  `lama_studi` int(10) NOT NULL,
  `ipk` float NOT NULL,
  `lama_lap` int(10) NOT NULL,
  `judul_ta` varchar(100) NOT NULL,
  `nilai_TA` int(10) NOT NULL,
  `judul_PKL` varchar(100) NOT NULL,
  `nilai_PKL` int(10) NOT NULL,
  `dp1` enum('0','1','2') DEFAULT NULL COMMENT '0=Hendra Yufit S.Kom,M.Cs; 1=Dwi Putro Sarwo; 2=Ganang Aji S.st,M.Kom; ',
  `dp2` enum('0','1','2') DEFAULT NULL COMMENT '0=Hendra Yufit S.Kom,M.Cs; 1=Dwi Putro Sarwo; 2=Ganang Aji S.st,M.Kom; ',
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `angkatan` int(4) NOT NULL,
  `ip_1` float NOT NULL,
  `ip_2` float NOT NULL,
  `ip_3` float NOT NULL,
  `ip_4` float NOT NULL,
  `ip_5` float NOT NULL,
  `ip_6` float NOT NULL,
  `upload_bebas_tanggungan` varchar(50) NOT NULL,
  `upload_ukt` varchar(50) NOT NULL,
  `upload_bta` varchar(50) NOT NULL,
  `upload_bpkl` varchar(50) NOT NULL,
  `upload_ta` varchar(50) NOT NULL,
  `upload_pkl` varchar(50) NOT NULL,
  `upload_sementara` varchar(50) NOT NULL,
  `upload_btp` varchar(50) NOT NULL,
  `upload_isma` varchar(50) NOT NULL,
  `upload_pf` varchar(50) NOT NULL,
  `nama_perusahaan` varchar(50) NOT NULL,
  `website_perusahaan` varchar(50) NOT NULL,
  `instansi_tempat_kerja` varchar(50) NOT NULL,
  `tahun_masuk_kerja` char(4) NOT NULL,
  `posisi_jabatan_saat_ini` varchar(15) NOT NULL,
  `status_akademik` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=mhs; 1=alumni',
  `status_alumni` enum('Bekerja','Studi Lanjut','Wirausaha','Opsional') NOT NULL,
  `nama_pt` varchar(100) NOT NULL,
  `kota` varchar(100) NOT NULL,
  `negara` varchar(50) NOT NULL,
  `j_pendidikan` varchar(10) NOT NULL,
  `bidang_studi` varchar(50) NOT NULL,
  `biaya` varchar(10) NOT NULL,
  `nama_usaha` varchar(50) NOT NULL,
  `alamat_usaha` varchar(50) NOT NULL,
  `website_usaha` varchar(50) NOT NULL,
  `bidang_usaha` varchar(50) NOT NULL,
  `tahun_berdiri` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_mahasiswa`
--

INSERT INTO `tb_mahasiswa` (`id`, `email`, `username`, `password`, `nim`, `nama`, `nama_panggilan`, `jenis_kelamin`, `agama`, `ket_agama`, `tempat_lahir`, `tanggal_lahir`, `berat_badan`, `tinggi_badan`, `tmp_alamat`, `tmp_telepon`, `ort_nama_ayah`, `status_pendaftaran`, `status_biodata`, `status_verifikasi`, `status_seleksi`, `status_perkawinan`, `fakultas`, `jur_prodi`, `tanggal_lulus`, `lama_studi`, `ipk`, `lama_lap`, `judul_ta`, `nilai_TA`, `judul_PKL`, `nilai_PKL`, `dp1`, `dp2`, `updated_at`, `created_at`, `angkatan`, `ip_1`, `ip_2`, `ip_3`, `ip_4`, `ip_5`, `ip_6`, `upload_bebas_tanggungan`, `upload_ukt`, `upload_bta`, `upload_bpkl`, `upload_ta`, `upload_pkl`, `upload_sementara`, `upload_btp`, `upload_isma`, `upload_pf`, `nama_perusahaan`, `website_perusahaan`, `instansi_tempat_kerja`, `tahun_masuk_kerja`, `posisi_jabatan_saat_ini`, `status_akademik`, `status_alumni`, `nama_pt`, `kota`, `negara`, `j_pendidikan`, `bidang_studi`, `biaya`, `nama_usaha`, `alamat_usaha`, `website_usaha`, `bidang_usaha`, `tahun_berdiri`) VALUES
(1, 'danny.bagus@stmik-aub.ac.id', 'danny', 'XBCCg4Bm', '216720824', 'Danny Bagus Saputra', 'Danny', 'L', '1', NULL, NULL, NULL, '54', '170', 'Gemolong, Sragen', '-', 'Sarjono', '1', '0', '0', '0', 'S', '', 'Sistem Informasi', '2016-11-09', 4, 3, 5, 'Aplikasi Wisata Kota Surakarta', 0, '', 0, '0', '0', '2016-11-03 15:43:03', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', 'file_1510024020.jpg', 'fghjk', 'sdfghj', 'efgh', '2011', 'tyui', '1', 'Wirausaha', '', '', '', '', '', '', 'cv kangen', 'agagagagagagag', 'hahahahah', 'teknologi informasi', '1212'),
(2, 'surya.kristian@stmik-aub.ac.id', 'surya', 'cw73T5RM', '216720825', 'Surya Kristian Atmaja', 'Surya', 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '0', '0', 'S', '', '', '0000-00-00', 0, 0, 0, '', 0, '', 0, '0', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', ''),
(4, 'imam.utomo@stmik-aub.ac.id', 'imam', 'Px06YFCU', '216720829', 'Ronny Imam Utomo', 'Imam', 'L', '1', NULL, 'Surakarta', '1997-10-30', '56', '170', 'Wirengan, Surakarta', '-', 'Suparjono', '1', '1', '0', '0', 'S', '', 'Sistem Informasi', '2016-11-09', 78, 3.25, 7, '1', 0, 'cok', 0, '2', '1', '2016-11-08 17:06:07', '0000-00-00 00:00:00', 0, 12, 10, 10, 10, 10, 1, 'IMG_0082.JPG', '', '', '', '', '', '', '', '', '', 'PT Cinta Abadi clalue', 'Cintaabadi.com', 'Perseroan', '2016', 'Predir cuy', '0', 'Bekerja', '', '', '', '', '', '', '', '', '', '', ''),
(5, 'zerubabel.putro@stmik-aub.ac.id', 'zputro', 'YcmK3XA3', '216720830', 'Zerubabel Tobat Putro', 'zputro', 'L', '2', NULL, 'q', '2014-06-10', '1', '23', 'asdxcv ', '3456', 'kijhg', '1', '1', '0', '0', 'S', '', '', '0000-00-00', 2, 87, 87, '1', 0, 'wkwk', 0, '2', '2', '2016-11-12 01:18:21', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', 'file_1478926445.jpg', '', '', '', '', '', '', '', '0', 'Studi Lanjut', '', '', '', '', '', '', '', '', '', '', ''),
(6, 'naufal.maarif@stmik-aub.ac.id', 'naufal', '32pwx8mI', '216720832', 'Muhammad Naufal Maarif', 'Naufal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '0', '0', NULL, '', '', '0000-00-00', 0, 0, 0, '', 0, '', 0, NULL, NULL, '2016-10-11 06:37:21', '2016-10-11 06:37:21', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', ''),
(7, 'hendri.setiawan@stmik-aub.ac.id', 'hendri', 'C91MTg55', '216720831', 'Hendri Setiawan', 'Hendri', 'L', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '0', '0', NULL, '', '', '0000-00-00', 0, 0, 0, '', 0, '', 0, NULL, NULL, '2016-10-11 16:24:09', '2016-10-11 16:24:09', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', ''),
(8, 'emanuel.lamanda@stmik-aub.ac.id', 'elamanda', 'gEWyJ82G', '216720833', 'Emanuel Lamanda Pasca', 'Elamanda', 'L', '2', NULL, 'asasas', '2010-06-15', '67', '987', 'cm', '4567', 'Ayah', '1', '1', '0', '0', 'S', '', '', '2015-07-15', 9, 3, 67, '2', 0, '', 0, '1', '2', '2016-10-11 17:06:49', '2016-10-11 17:05:42', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', ''),
(9, 'alfian.kusuma@stmik-aub.ac.id', 'alfian', 'D9MyqXps', '216720834', 'Alfian Rys Kusuma A.', 'Alfian', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '0', '0', NULL, '', '', '0000-00-00', 0, 0, 0, '', 0, '', 0, NULL, NULL, '2016-10-13 08:39:54', '2016-10-13 08:39:54', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', ''),
(10, 'muslim.nugroho@stmik-aub.ac.id', 'muslim', 'fB2TQpWN', '216720835', 'Muslim Nugroho', 'Muslim', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '0', '0', NULL, '', '', '0000-00-00', 0, 0, 0, '', 0, '', 0, NULL, NULL, '2016-10-18 06:37:36', '2016-10-18 06:37:36', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', ''),
(11, 'endras.rosyid@stmik-aub.ac.id', 'endras', 'rBvYMrN0', '21670836', 'Endras Nur Rosyid', 'Endras', 'L', '0', NULL, 'London', '1996-01-01', '70', '170', 'Jl. Ni Aja Dulu', '088888888', 'Mandra', '1', '1', '0', '0', 'B', '', '', '2017-06-17', 8, 9, 11, '1', 0, '', 0, '2', '2', '2016-10-26 16:59:59', '2016-10-26 16:57:12', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', ''),
(12, 'abdul.aziz@stmik-aub.ac.id', 'aziz', 'iuONjzoc', '21670828', 'Muhammad Abdul Aziz', 'Aziz', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', '0', '0', NULL, '', '', '0000-00-00', 0, 0, 0, '', 0, '2', 0, NULL, NULL, '2017-11-02 23:10:09', '2017-10-31 03:01:42', 2012, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', 'Bekerja', '', '', '', '', '', '', '', '', '', '', ''),
(13, 'dwi.pujiyanto', 'dpujiyanto', '16C21jf7', '21670852', 'Dwi Pujiyanto', 'Pujiyanto', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '0', '0', NULL, '', '', '0000-00-00', 0, 0, 0, '', 0, '', 0, NULL, NULL, '2017-11-01 18:52:15', '2017-11-01 18:52:15', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', 'Bekerja', '', '', '', '', '', '', '', '', '', '', ''),
(14, 'marcellinus.yosia@stmik-aub.ac.id', 'marcell', 'xeMhCu5d', '21671862', 'Marcellinus Yosia I.', 'Marcell', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '0', '0', NULL, '', '', '0000-00-00', 0, 0, 0, '', 0, '', 0, NULL, NULL, '2017-11-02 20:55:41', '2017-11-02 20:55:41', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', 'Bekerja', '', '', '', '', '', '', '', '', '', '', ''),
(15, 'fadhil@stmik-aub.ac.id', 'fadhil', 'GkDoS746', '216720826', 'Muhammad Fadhil', 'Fadhil', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '0', '0', NULL, '', '', '0000-00-00', 0, 0, 0, '', 0, '', 0, NULL, NULL, '2017-11-13 12:00:49', '2017-11-13 12:00:49', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', 'file_1510573818.jpg', '', '', '', '', '', '1', 'Bekerja', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengumuman`
--

CREATE TABLE `tb_pengumuman` (
  `id` smallint(4) UNSIGNED NOT NULL,
  `judul` varchar(64) NOT NULL,
  `slug` varchar(128) NOT NULL,
  `isi` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_pengumuman`
--

INSERT INTO `tb_pengumuman` (`id`, `judul`, `slug`, `isi`, `created_at`, `updated_at`) VALUES
(10, 'LOWONGAN PT. VISIONE SYSTEM', 'lowongan-pt-visione-system', '<p >PT. VISIONE SYSTEM is a rapid growing IT solution. We are mainly focus on Cloud Solution, enterprise application and e-commerce.</p>\r <p >We are currently looking for lead programmer, the one that should be great in analytical and also a team member.</p>\r <p ><strong>&nbsp;</strong><strong>JOB VACANCY PT. VISIONE SYSTEM</strong></p>\r <p ><strong>Web Designer</strong><strong> (Code : WD) &ndash; Fulltime</strong></p>\r <p >Qualification :</p>\r\n<ul>\r <li >Min education D3 or S1</li>\r <li >Excellent knowledge of HTML, CSS, Flash and ActionScript</li>\r <li >Familiar with node.js</li>\r <li >Having knowledge in Design Software, such as Dreamweaver, Flash, PhotoShop, Adobe Ilustrator or CorelDraw, etc</li>\r <li >Good analytical &amp; conceptual thinking and creative &amp; innovative skills</li>\r <li ><strong>Submit sample web ( .jpg or url ) &hellip; Please Attached </strong></li>\r\n</ul>\r <p ><strong>Senior Mobile Progammer (Code : SMP) &ndash; Fulltime</strong></p>\r <p >Qualification :</p>\r\n<ul>\r <li >Min education D3 or S1</li>\r <li >Experience in building mobile application IOS, Android (windows platform is an added value)</li>\r <li >Can use xcode and IBM mobile first platform</li>\r <li >Familiar with IOS SDK and android SDK (windows platform is an added value)</li>\r <li >Understand MVVM / MVC / MVP Pattern</li>\r <li >Understand MongoDB</li>\r <li >Familiar with node.js</li>\r <li >Familiar with Linux OS</li>\r <li >Able to show 1 mobile apps that already developed is an added value</li>\r <li >Understand IOS and Android Lifecycle (windows platform is an added value)</li>\r <li >Good skill on javascript, CSS, HTML</li>\r <li >Good understanding on UI framework (angular, react, etc)</li>\r <li ><strong>Submi</strong><strong>t</strong><strong> sample </strong><strong>project</strong><strong> ( .jpg or url ) &hellip; Please Attached </strong></li>\r\n</ul>\r <p ><strong>PHP Programmer (Code : PP) </strong><strong>&ndash; Fulltime</strong></p>\r <p >Qualification :</p>\r\n<ul>\r <li >D3/ S1 Computer Science or Information Systems with minimum GPA 75 of 4.00</li>\r <li >Having knowledge in MySql</li>\r <li >Familiar with HTML; CSS;&nbsp; Javascript (Ajax);&nbsp; Jquery+PHP 5.0; MySQL</li>\r <li >Familiar with Code Igniter Framework</li>\r <li >Familiar with Linux OS</li>\r <li >Deep knowledge in Programming Language PHP</li>\r <li >Good analytical &amp; conceptual thinking and creative &amp; innovative skills</li>\r <li ><strong>Submit sample web ( .jpg or url ) &hellip; Please Attached </strong></li>\r\n</ul>\r <p ><strong>&nbsp;</strong><strong>Java</strong> <strong>Progammer (Code : JP)</strong></p>\r <p >Qualification :</p>\r\n<ul>\r <li >S1 with minimum GPA 75 of 4.00</li>\r <li >Fresh Graduates are welcome, if any project experience</li>\r <li >Deep knowledge in Programming Language JAVA</li>\r <li >Having knowledge in Spring and Hibernate architecture is clear advantage</li>\r <li >Have deep knowledge in MySql, Sql Server, RDBMS, J2EE, J2SE</li>\r <li >Understand MongoDB</li>\r <li >Familiar with node.js</li>\r <li >Familiar with Linux OS</li>\r <li ><strong>Submit sample web ( .jpg or url ) &hellip; Please Attached </strong></li>\r\n</ul>\r\n<p>&nbsp;</p>\r <p >Facilities :</p>\r\n<ul>\r <li >Good basic salary</li>\r <li >bonus based on performance</li>\r\n</ul>\r <p >If you feel comfortable for above position, please submit your CV &amp; Supporting doc.</p>\r <p >to <strong><a href=\"mailto:hrd@visione-system.com\">hrd@visione-system.com</a></strong> or post it to PT. <strong>VISIONE SYSTEM </strong></p>\r <p ><strong>Jl. </strong><strong>Pakel, Ruko Mulyo Mandiri No. 9, Banyuanyar, Solo 57137</strong></p>\r <p >(Not later than October 31<sup>th</sup> 2017)</p>', '2017-10-20 17:35:55', '2017-10-29 11:08:23'),
(11, 'Cara Update Data Alumni', 'cara-update-data', '<p>Kepada alumni/calon wisudawan dimohon kesediaannya untuk mengisi/mengupdate biodata dengan langkah-langkah berikut ini,<br/><ul><li>Login, lakukan proses login</li>\r\n<li>Pengisian Data Alumni, meliputi biodata, pekerjaan, keahlian</li><li>Simpan Data</li></ul></p>', '2017-10-19 17:26:16', '2017-10-10 17:26:16'),
(12, 'Verifikasi Data Alumni', 'verifikasi-data-alumni', '<p>Diberitahukan kepada alumni yang belum melakukan verifikasi data sampai tanggal 7 Oktober 2017, maka proses verifikasi data baru dapat dilakukan mulai tanggal 15 s.d 18 Oktober 2017. Ijasah dan trankrip akan dicetak setelah data verifikasi selesai dilakukan oleh alumni.</p><p>\r\nDemikian disampaikan, atas perhatiannya diucapkan terima kasih.</p>', '2017-10-16 06:14:14', '2017-10-30 23:38:48'),
(13, 'LOWONGAN PT CITRA WARNA ABADI', 'lowongan-pt-citra-warna-abadi', '<p>Citra Warna Abadi merupakan industri cat yang berada di Sukoharjo, dengan melihat perkembangan industri cat di Indonesia yang sangat pesat maka kami berkeinginan untuk dapat diterima konsumen sebagai produk lokal berkualitas impor. Berangkat dari alasan tersebut, kami senantiasa berusaha meningkatkan kualitas produk dengan menggunakan tenaga yang potensial dan professional dalam semua aspek penting dan memegang peranan besar dalam perkembangan. Berikut ini merupakan kebutuhan karyawan dalam perusahaan ..</p>', '2017-11-13 19:25:58', '2017-11-13 19:30:23');

-- --------------------------------------------------------

--
-- Table structure for table `tb_studi_lanjut`
--

CREATE TABLE `tb_studi_lanjut` (
  `id_studi` varchar(10) NOT NULL,
  `nim` varchar(9) NOT NULL,
  `nama_perguruan_tinggi` varchar(100) NOT NULL,
  `kota` varchar(100) NOT NULL,
  `negara` varchar(100) NOT NULL,
  `jenjang_pendidikan` varchar(100) NOT NULL,
  `bidang_studi_yang_diambil` varchar(100) NOT NULL,
  `sumber_biaya_studi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` tinyint(2) UNSIGNED NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` char(32) NOT NULL,
  `nama` varchar(32) NOT NULL,
  `level` enum('operator','administrator') NOT NULL DEFAULT 'operator',
  `is_blokir` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `username`, `password`, `nama`, `level`, `is_blokir`, `created_at`, `updated_at`) VALUES
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Administrator', 'administrator', '0', '0000-00-00 00:00:00', '2014-12-10 23:49:22'),
(13, 'royan', 'e19cb7c038c2159012047e8b276bb6a2', 'Royan Setyawan', 'operator', '0', '2014-12-20 14:39:41', '2014-12-30 04:32:40'),
(14, 'brian', '71c7c97e86a4618374f3a345248b3dff', 'Brian Bangkit', 'operator', '0', '2014-12-30 04:48:30', '2014-12-30 04:48:30');

-- --------------------------------------------------------

--
-- Table structure for table `tb_wirausaha`
--

CREATE TABLE `tb_wirausaha` (
  `id_wirausaha` varchar(10) NOT NULL,
  `nim` varchar(9) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `alamat_perusahaan` varchar(100) NOT NULL,
  `website_perusahaan` varchar(100) NOT NULL,
  `bidang_usaha` varchar(100) NOT NULL,
  `tahun_perusahaan_berdiri` text NOT NULL,
  `jumlah_karyawan` int(11) NOT NULL,
  `omset_perbulan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_kerja`
--
ALTER TABLE `tb_kerja`
  ADD PRIMARY KEY (`id_kerja`);

--
-- Indexes for table `tb_kontak`
--
ALTER TABLE `tb_kontak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_mahasiswa`
--
ALTER TABLE `tb_mahasiswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_pengumuman`
--
ALTER TABLE `tb_pengumuman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_studi_lanjut`
--
ALTER TABLE `tb_studi_lanjut`
  ADD PRIMARY KEY (`id_studi`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_wirausaha`
--
ALTER TABLE `tb_wirausaha`
  ADD PRIMARY KEY (`id_wirausaha`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_kerja`
--
ALTER TABLE `tb_kerja`
  MODIFY `id_kerja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_kontak`
--
ALTER TABLE `tb_kontak`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_mahasiswa`
--
ALTER TABLE `tb_mahasiswa`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tb_pengumuman`
--
ALTER TABLE `tb_pengumuman`
  MODIFY `id` smallint(4) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
