-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 01, 2017 at 03:39 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alumni5`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_kerja`
--

CREATE TABLE `tb_kerja` (
  `id_kerja` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `nim` varchar(9) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `website_perusahaan` varchar(100) NOT NULL,
  `instansi_tempat_kerja` varchar(100) NOT NULL,
  `tahun_masuk_kerja` char(4) NOT NULL,
  `posisi_jabatan_saat_ini` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kerja`
--

INSERT INTO `tb_kerja` (`id_kerja`, `id`, `nim`, `nama_perusahaan`, `website_perusahaan`, `instansi_tempat_kerja`, `tahun_masuk_kerja`, `posisi_jabatan_saat_ini`, `updated_at`) VALUES
(1, 4, 'E31141753', 'PT Cinta Abadi clalue', 'Cintaabadi.com', 'Perseroan', '2016', 'Predir cuy', '2016-11-02 19:34:38');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kontak`
--

CREATE TABLE `tb_kontak` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `judul` varchar(128) NOT NULL,
  `isi` text NOT NULL,
  `is_dibalas` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=belum dibalas; 1=sudah dibalas',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_kontak`
--

INSERT INTO `tb_kontak` (`id`, `nama`, `email`, `judul`, `isi`, `is_dibalas`, `created_at`, `updated_at`) VALUES
(1, 'awan pribadi basuki', 'awan@gmail.com', 'Tidak Bisa Login', 'Saya tidak bisa login.', '0', '2014-12-09 20:35:17', '2014-12-09 20:35:17'),
(2, 'Awan Pribadi Basuki', 'awan@localhost.com', 'Tidak Bisa Login', 'Saya tidak bisa login, username dan password yang diberikan saya lupa.', '0', '2014-12-10 17:57:24', '2014-12-10 17:57:24'),
(3, 'Awan Pribadi Basuki', 'awan@localhost.com', 'Tidak bisa login', 'Saya tidak bisa login, bagaimana ya?', '0', '2014-12-15 23:34:33', '2014-12-15 23:34:33');

-- --------------------------------------------------------

--
-- Table structure for table `tb_mahasiswa`
--

CREATE TABLE `tb_mahasiswa` (
  `id` int(10) NOT NULL,
  `email` varchar(64) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` char(8) NOT NULL,
  `nim` varchar(9) NOT NULL,
  `nama` varchar(64) NOT NULL,
  `nama_panggilan` varchar(32) NOT NULL,
  `jenis_kelamin` enum('P','L') DEFAULT NULL,
  `agama` enum('0','1','2','3','4','5','6') DEFAULT NULL COMMENT '0=lainnya; 1=islam; 2=katolik; 3=protestan; 4=hindu; 5=budha; 6=Konghucu',
  `ket_agama` varchar(32) DEFAULT NULL,
  `tempat_lahir` varchar(32) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `berat_badan` varchar(3) DEFAULT NULL,
  `tinggi_badan` varchar(3) DEFAULT NULL,
  `tmp_alamat` varchar(255) DEFAULT NULL,
  `tmp_telepon` varchar(16) DEFAULT NULL,
  `ort_nama_ayah` varchar(64) DEFAULT NULL,
  `status_pendaftaran` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=mengundurkan diri; 1=aktif',
  `status_biodata` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=belum lengkap; 1=sudah lengkap',
  `status_verifikasi` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=belum; 1=sudah',
  `status_seleksi` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 = tidak lulus; 1=lulus;',
  `status_perkawinan` enum('S','B') DEFAULT NULL,
  `fakultas` varchar(35) NOT NULL,
  `jur_prodi` varchar(100) NOT NULL,
  `tanggal_lulus` date NOT NULL,
  `lama_studi` int(10) NOT NULL,
  `ipk` float NOT NULL,
  `lama_lap` int(10) NOT NULL,
  `judul_ta` varchar(100) NOT NULL,
  `nilai_TA` int(10) NOT NULL,
  `judul_PKL` varchar(100) NOT NULL,
  `nilai_PKL` int(10) NOT NULL,
  `dp1` enum('0','1','2') DEFAULT NULL COMMENT '0=Hendra Yufit S.Kom,M.Cs; 1=Dwi Putro Sarwo; 2=Ganang Aji S.st,M.Kom; ',
  `dp2` enum('0','1','2') DEFAULT NULL COMMENT '0=Hendra Yufit S.Kom,M.Cs; 1=Dwi Putro Sarwo; 2=Ganang Aji S.st,M.Kom; ',
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `angkatan` int(4) NOT NULL,
  `ip_1` float NOT NULL,
  `ip_2` float NOT NULL,
  `ip_3` float NOT NULL,
  `ip_4` float NOT NULL,
  `ip_5` float NOT NULL,
  `ip_6` float NOT NULL,
  `upload_bebas_tanggungan` varchar(50) NOT NULL,
  `upload_ukt` varchar(50) NOT NULL,
  `upload_bta` varchar(50) NOT NULL,
  `upload_bpkl` varchar(50) NOT NULL,
  `upload_ta` varchar(50) NOT NULL,
  `upload_pkl` varchar(50) NOT NULL,
  `upload_sementara` varchar(50) NOT NULL,
  `upload_btp` varchar(50) NOT NULL,
  `upload_isma` varchar(50) NOT NULL,
  `upload_pf` varchar(50) NOT NULL,
  `nama_perusahaan` varchar(50) NOT NULL,
  `website_perusahaan` varchar(50) NOT NULL,
  `instansi_tempat_kerja` varchar(50) NOT NULL,
  `tahun_masuk_kerja` char(4) NOT NULL,
  `posisi_jabatan_saat_ini` varchar(15) NOT NULL,
  `status_akademik` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0=mhs; 1=alumni',
  `status_alumni` enum('Bekerja','Studi Lanjut','Wirausaha','Opsional') NOT NULL,
  `nama_pt` varchar(100) NOT NULL,
  `kota` varchar(100) NOT NULL,
  `negara` varchar(50) NOT NULL,
  `j_pendidikan` varchar(10) NOT NULL,
  `bidang_studi` varchar(50) NOT NULL,
  `biaya` varchar(10) NOT NULL,
  `nama_usaha` varchar(50) NOT NULL,
  `alamat_usaha` varchar(50) NOT NULL,
  `website_usaha` varchar(50) NOT NULL,
  `bidang_usaha` varchar(50) NOT NULL,
  `tahun_berdiri` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_mahasiswa`
--

INSERT INTO `tb_mahasiswa` (`id`, `email`, `username`, `password`, `nim`, `nama`, `nama_panggilan`, `jenis_kelamin`, `agama`, `ket_agama`, `tempat_lahir`, `tanggal_lahir`, `berat_badan`, `tinggi_badan`, `tmp_alamat`, `tmp_telepon`, `ort_nama_ayah`, `status_pendaftaran`, `status_biodata`, `status_verifikasi`, `status_seleksi`, `status_perkawinan`, `fakultas`, `jur_prodi`, `tanggal_lulus`, `lama_studi`, `ipk`, `lama_lap`, `judul_ta`, `nilai_TA`, `judul_PKL`, `nilai_PKL`, `dp1`, `dp2`, `updated_at`, `created_at`, `angkatan`, `ip_1`, `ip_2`, `ip_3`, `ip_4`, `ip_5`, `ip_6`, `upload_bebas_tanggungan`, `upload_ukt`, `upload_bta`, `upload_bpkl`, `upload_ta`, `upload_pkl`, `upload_sementara`, `upload_btp`, `upload_isma`, `upload_pf`, `nama_perusahaan`, `website_perusahaan`, `instansi_tempat_kerja`, `tahun_masuk_kerja`, `posisi_jabatan_saat_ini`, `status_akademik`, `status_alumni`, `nama_pt`, `kota`, `negara`, `j_pendidikan`, `bidang_studi`, `biaya`, `nama_usaha`, `alamat_usaha`, `website_usaha`, `bidang_usaha`, `tahun_berdiri`) VALUES
(1, 'gan@gmail.com', 'ernes', 'XBCCg4Bm', 'E31141794', 'su', 'Ernes', 'L', '6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '0', '0', 'S', '', '', '0000-00-00', 0, 0, 0, '', 0, '', 0, '0', '0', '2016-11-03 15:43:03', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', 'fghjk', 'sdfghj', 'efgh', '2011', 'tyui', '1', 'Wirausaha', '', '', '', '', '', '', 'cv kangen', 'agagagagagagag', 'hahahahah', 'teknologi informasi', '1212'),
(2, 'hannahrfhd@gmail.com', 'annisaha', 'cw73T5RM', 'e31141714', 'Annisa Hanna Rufaidah', 'Hanna', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '0', '0', 'S', '', '', '0000-00-00', 0, 0, 0, '', 0, '', 0, '0', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', ''),
(3, 'ganang@gmail.com', 'ganangaj', 'J2ZCI9oy', 'E31141794', 'Ganang Aji Pambudhi', 'Agan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '0', '0', 'S', '', '', '0000-00-00', 0, 0, 0, '', 0, '', 0, '0', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', 'file_1478937718.png', 'file_1478938171.png', 'file_1478931063.jpg', '', '', '', '', '', '1', '', '', '', '', '', '', '', '', '', '', '', ''),
(4, 'ainurrofiq143@gmail.com', 'ainurrfq', 'Px06YFCU', 'E31141753', 'Moh. Ainur Rofiq', 'Rofiq', 'L', '4', NULL, 'okijhc', '2018-10-30', '23', '345', 'kijhgvchujhgv', '09876', 'uuuuu', '1', '1', '0', '0', 'S', '', '', '2017-06-17', 34, 91, 7, '1', 0, 'cok', 0, '2', '1', '2016-11-08 17:06:07', '0000-00-00 00:00:00', 0, 12, 10, 10, 10, 10, 1, 'IMG_0082.JPG', '', '', '', '', '', '', '', '', '', 'PT Cinta Abadi clalue', 'Cintaabadi.com', 'Perseroan', '2016', 'Predir cuy', '0', 'Bekerja', '', '', '', '', '', '', '', '', '', '', ''),
(5, 'aji.com', 'ganang', 'YcmK3XA3', 'E31141791', 'Ganang ', 'gan', 'P', '2', NULL, 'q', '2014-06-10', '1', '23', 'asdxcv ', '3456', 'kijhg', '1', '1', '0', '0', 'S', '', '', '0000-00-00', 2, 87, 87, '1', 0, 'wkwk', 0, '2', '2', '2016-11-12 01:18:21', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', 'file_1478926445.jpg', '', '', '', '', '', '', '', '0', 'Studi Lanjut', '', '', '', '', '', '', '', '', '', '', ''),
(6, 'admin.blen@blew.com', 'RandyBlen', '32pwx8mI', 'E31141626', 'Randy Achmad Pujianto', 'Blen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '0', '0', NULL, '', '', '0000-00-00', 0, 0, 0, '', 0, '', 0, NULL, NULL, '2016-10-11 06:37:21', '2016-10-11 06:37:21', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', ''),
(7, 'laras.com', 'laras', 'C91MTg55', 'E11111111', 'laras baper', 'yayas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '0', '0', NULL, '', '', '0000-00-00', 0, 0, 0, '', 0, '', 0, NULL, NULL, '2016-10-11 16:24:09', '2016-10-11 16:24:09', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', ''),
(8, 'uio', 'qqqqq', 'gEWyJ82G', 'e31141087', 'yuio', 'fvb ', 'L', '2', NULL, 'asasas', '2010-06-15', '67', '987', 'cm', '4567', 'Ayah', '1', '1', '0', '0', 'S', '', '', '2015-07-15', 9, 3, 67, '2', 0, '', 0, '1', '2', '2016-10-11 17:06:49', '2016-10-11 17:05:42', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', ''),
(9, 'tahta@gmail.com', 'tahtaid', 'D9MyqXps', 'E31141999', 'Tahta Dewanata', 'Tahta', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '0', '0', NULL, '', '', '0000-00-00', 0, 0, 0, '', 0, '', 0, NULL, NULL, '2016-10-13 08:39:54', '2016-10-13 08:39:54', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', ''),
(10, 'ghaSGAg', 'gjhgsdj', 'fB2TQpWN', '111111111', 'hgasjhgAJHGS', 'JASJAkjsg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '0', '0', NULL, '', '', '0000-00-00', 0, 0, 0, '', 0, '', 0, NULL, NULL, '2016-10-18 06:37:36', '2016-10-18 06:37:36', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', ''),
(11, 'wkwk@gmail.crot', 'wkwkwk', 'rBvYMrN0', 'E31141201', 'Maman', 'Jason', 'L', '0', NULL, 'London', '1996-01-01', '70', '170', 'Jl. Ni Aja Dulu', '088888888', 'Mandra', '1', '1', '0', '0', 'B', '', '', '2017-06-17', 8, 9, 11, '1', 0, '', 0, '2', '2', '2016-10-26 16:59:59', '2016-10-26 16:57:12', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', ''),
(12, 'ernes.cahyo@gmail.com', 'budi', 'iuONjzoc', 'J20132001', 'budi cahyono', 'budi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '0', '0', NULL, '', '', '0000-00-00', 0, 0, 0, '', 0, '', 0, NULL, NULL, '2017-10-31 03:01:42', '2017-10-31 03:01:42', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', 'Bekerja', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengumuman`
--

CREATE TABLE `tb_pengumuman` (
  `id` smallint(4) UNSIGNED NOT NULL,
  `judul` varchar(64) NOT NULL,
  `slug` varchar(128) NOT NULL,
  `isi` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tb_pengumuman`
--

INSERT INTO `tb_pengumuman` (`id`, `judul`, `slug`, `isi`, `created_at`, `updated_at`) VALUES
(10, 'Mengetahui Status Kelulusan Siswa PSB 2014', 'mengetahui-status-kelulusan-siswa-psb-2014', '<p>&nbsp;Berikut ini adalah cara verifikasi data PSB 2014.&nbsp; Berikut ini adalah cara verifikasi data PSB 2014. &nbsp;Berikut ini adalah cara verifikasi data PSB 2014. &nbsp; Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014.</p>\r\n<p>&nbsp;Berikut ini adalah cara verifikasi data PSB 2014.&nbsp; Berikut ini adalah cara verifikasi data PSB 2014. &nbsp;Berikut ini adalah cara verifikasi data PSB 2014. &nbsp; Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014.</p>\r\n<p>&nbsp;Berikut ini adalah cara verifikasi data PSB 2014.&nbsp; Berikut ini adalah cara verifikasi data PSB 2014. &nbsp;Berikut ini adalah cara verifikasi data PSB 2014. &nbsp; Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014.</p>\r\n<p>&nbsp;Berikut ini adalah cara verifikasi data PSB 2014.&nbsp; Berikut ini adalah cara verifikasi data PSB 2014. &nbsp;Berikut ini adalah cara verifikasi data PSB 2014. &nbsp; Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014.</p>\r\n<p>&nbsp;Berikut ini adalah cara verifikasi data PSB 2014.&nbsp; Berikut ini adalah cara verifikasi data PSB 2014. &nbsp;Berikut ini adalah cara verifikasi data PSB 2014. &nbsp; Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014.</p>\r\n<p>&nbsp;Berikut ini adalah cara verifikasi data PSB 2014.&nbsp; Berikut ini adalah cara verifikasi data PSB 2014. &nbsp;Berikut ini adalah cara verifikasi data PSB 2014. &nbsp; Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014.</p>\r\n<p>&nbsp;Berikut ini adalah cara verifikasi data PSB 2014.&nbsp; Berikut ini adalah cara verifikasi data PSB 2014. &nbsp;Berikut ini adalah cara verifikasi data PSB 2014. &nbsp; Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014.</p>\r\n<p>&nbsp;Berikut ini adalah cara verifikasi data PSB 2014.&nbsp; Berikut ini adalah cara verifikasi data PSB 2014. &nbsp;Berikut ini adalah cara verifikasi data PSB 2014. &nbsp; Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014. Berikut ini adalah cara verifikasi data PSB 2014.</p>', '2014-12-05 17:35:55', '2014-12-05 19:58:56'),
(11, 'Cara Update Data', 'cara-update-data', '<p>Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini.</p>\r\n<p>Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini.</p>\r\n<p>Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini.</p>\r\n<p>Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini.</p>\r\n<p>Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini.</p>\r\n<p>Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini.</p>\r\n<p>Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini.</p>\r\n<p>Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini.</p>\r\n<p>Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini.</p>\r\n<p>Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini. Bagi anda yang belum tahu cara mengupdate data, maka ikuti langkah berikut ini.</p>', '2014-12-08 17:26:16', '2014-12-08 17:26:16'),
(12, 'Hallo Dunia', 'hallo-dunia', '<p>Hallo Dunia</p>', '2014-12-31 06:14:14', '2017-10-30 23:38:48');

-- --------------------------------------------------------

--
-- Table structure for table `tb_studi_lanjut`
--

CREATE TABLE `tb_studi_lanjut` (
  `id_studi` varchar(10) NOT NULL,
  `nim` varchar(9) NOT NULL,
  `nama_perguruan_tinggi` varchar(100) NOT NULL,
  `kota` varchar(100) NOT NULL,
  `negara` varchar(100) NOT NULL,
  `jenjang_pendidikan` varchar(100) NOT NULL,
  `bidang_studi_yang_diambil` varchar(100) NOT NULL,
  `sumber_biaya_studi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` tinyint(2) UNSIGNED NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` char(32) NOT NULL,
  `nama` varchar(32) NOT NULL,
  `level` enum('operator','administrator') NOT NULL DEFAULT 'operator',
  `is_blokir` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `username`, `password`, `nama`, `level`, `is_blokir`, `created_at`, `updated_at`) VALUES
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Administrator', 'administrator', '0', '0000-00-00 00:00:00', '2014-12-10 23:49:22'),
(13, 'awan', 'e19cb7c038c2159012047e8b276bb6a2', 'Awan Pribadi Basuki', 'operator', '0', '2014-12-20 14:39:41', '2014-12-30 04:32:40'),
(14, 'bangkit', '71c7c97e86a4618374f3a345248b3dff', 'Bangkit Pribadi', 'operator', '0', '2014-12-30 04:48:30', '2014-12-30 04:48:30'),
(15, 'ainurrfq', '848e1c8c31308490e8e299fbfd44a18d', 'Peck', 'operator', '0', '2016-09-27 05:43:19', '2016-09-27 05:43:19');

-- --------------------------------------------------------

--
-- Table structure for table `tb_wirausaha`
--

CREATE TABLE `tb_wirausaha` (
  `id_wirausaha` varchar(10) NOT NULL,
  `nim` varchar(9) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `alamat_perusahaan` varchar(100) NOT NULL,
  `website_perusahaan` varchar(100) NOT NULL,
  `bidang_usaha` varchar(100) NOT NULL,
  `tahun_perusahaan_berdiri` text NOT NULL,
  `jumlah_karyawan` int(11) NOT NULL,
  `omset_perbulan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_kerja`
--
ALTER TABLE `tb_kerja`
  ADD PRIMARY KEY (`id_kerja`);

--
-- Indexes for table `tb_kontak`
--
ALTER TABLE `tb_kontak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_mahasiswa`
--
ALTER TABLE `tb_mahasiswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_pengumuman`
--
ALTER TABLE `tb_pengumuman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_studi_lanjut`
--
ALTER TABLE `tb_studi_lanjut`
  ADD PRIMARY KEY (`id_studi`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_wirausaha`
--
ALTER TABLE `tb_wirausaha`
  ADD PRIMARY KEY (`id_wirausaha`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_kerja`
--
ALTER TABLE `tb_kerja`
  MODIFY `id_kerja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_kontak`
--
ALTER TABLE `tb_kontak`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_mahasiswa`
--
ALTER TABLE `tb_mahasiswa`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tb_pengumuman`
--
ALTER TABLE `tb_pengumuman`
  MODIFY `id` smallint(4) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
