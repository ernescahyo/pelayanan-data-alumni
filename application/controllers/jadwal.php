<?php
class Jadwal extends Public_Controller
{
    public $data = array(
        'halaman' => 'jadwal',
        'main_view' => 'jadwal',
        'title' => 'Jadwal',
    );

	function __construct(){
		parent::__construct();		
		$this->load->model('pengumuman_model');        
	}
	
    public function index()
    {
		$pengumuman = $this->pengumuman_model->get_pengumuman()->result();
		$this->data['home'] = $pengumuman;
        $this->load->view($this->layout, $this->data);
    }
}