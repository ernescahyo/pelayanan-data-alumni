<?php
class Home extends Public_Controller
{
    public $data = array(
        'halaman' => 'home',
        'main_view' => 'home'
    );
	
	function __construct(){
		parent::__construct();		
		$this->load->model('pengumuman_model');        
	}

    public function index()
    {	
	  	$pengumuman = $this->pengumuman_model->get_pengumuman()->result();
		$this->data['home'] = $pengumuman;
		$this->load->view($this->layout, $this->data);
    }
}