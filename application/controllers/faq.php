<?php
class Faq extends Public_Controller
{
    public $data = array(
        'halaman' => 'tanya jawab',
        'main_view' => 'faq',
        'title' => 'Tanya Jawab',
    );
	
	function __construct(){
		parent::__construct();		
		$this->load->model('pengumuman_model');        
	}
	
    public function index()
    {	
		$pengumuman = $this->pengumuman_model->get_pengumuman()->result();
		$this->data['home'] = $pengumuman;
        $this->load->view($this->layout, $this->data);
    }
}