<?php
class Bekerja extends Public_Controller
{
	public $data = array(
        'halaman' => 'bekerja',
        'main_view' => 'dashboard/bekerjav',
    );
	
	public function index() 
	{
			 $bekerja = $this->input->post(null, true);
        // Validasi.
        if ($this->bekerja->validate('form_rules')) {
            if ($this->bekerja->simpan($bekerja)) {
				
                $this->session->set_flashdata('pesanyudisium', 'Prayudisium berhasil disimpan. Kembali ke halaman ' . anchor('dashboard/home', 'home.', 'class="alert-link"'));
                redirect('dashboard/bekerja');
            } else {
                $this->session->set_flashdata('pesan_error', 'Maaf, penyimpanan Prayudisium gagal. Coba ' . anchor('dashboard/biodata', 'ulangi', 'class="alert-link"') .' beberapa saat lagi.');
                redirect('dashboard/prayudisium-error');
            }
        }
		
	        if (!$_POST) {
            $id_kerja = intval($this->session->userdata('no_peserta'));
            $this->data['values'] = (object) $this->bekerja->get($id_kerja );
        } else {
            $this->data['values'] = (object) $bekerja;
        }
        $this->data['form_action'] = site_url('dashboard/bekerja');
        $this->load->view($this->layout, $this->data);	
		
	}
	
}

?>