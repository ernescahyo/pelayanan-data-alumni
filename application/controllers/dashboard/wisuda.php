<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Wisuda extends Dashboard_Controller
{		
    var $limit=10;
    var $offset=10;
	var $table = 'tb_mahasiswa';
	var $data;
	
	/*public $data = array(
        'halaman' => 'wisuda',
        'main_view' => 'dashboard/fwisuda',
		'btp' => '',
    );*/
 
    public function __construct() {
        parent::__construct();
        $this->load->model('mwisuda'); //load model mwisuda yang berada di folder model
        $this->load->helper(array('url')); //load helper url
		$result = $this->mwisuda->get_data('$values')->row();
		$this->data = array(
			'halaman' => 'wisuda',
			'main_view' => 'dashboard/fwisuda',
			'btp' => $result->upload_btp,
			'isma' => $result->upload_isma,
			'pf' => $result->upload_pf,
		);
    }
	
	public function update_alumni()
	{
		$data = array(
                  'status_akademik' => '1',
                  'id' => $this->session->userdata('no_peserta'),
                );
		$this->mwisuda->get_update($data);
		redirect('dashboard/alumni');
	}
	
    public function index($page=NULL,$offset='',$key=NULL)
    {								
		
		$wisuda = $this->input->post(null, true);
	
		$this->load->library('session');
		
		$this->load->library('form_validation');
		 
		$this->form_validation->run();
		
        $data['titel']='Upload Berkas'; //varibel title
		
		if (!$_POST) {
            $id = intval($this->session->userdata('no_peserta'));
            //$this->data['values'] = (object) $this->wisuda->get($id);
        } else {
            $this->data['values'] = (object) $wisuda;
        }		
		
		$this->load->view($this->layout, $this->data);
	
	}
    public function add() {
         
        $data['titel']='Form Upload Berkas'; //varibel title
         
        //view yang tampil jika fungsi add diakses pada url
        $this->load->view('dashboard/prayudisium/wisuda',$data);
        
    }
	
    public function upload_btp(){
        $this->load->library('upload');
        $nmfile = "file_".time(); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './uploads/wisuda/'; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg|bmp|pdf'; //type yang dapat diakses bisa anda sesuaikan gif|jpg|png|jpeg|bmp
        $config['max_size'] = '10000'; //maksimum besar file 2M
        $config['file_name'] = $nmfile; //nama yang terupload nantinya
 
        $this->upload->initialize($config);
         
        if($_FILES['file_btp']['name'])
        {
            if ($this->upload->do_upload('file_btp'))
            {
                $gbr = $this->upload->data();
                $data = array(
                  'upload_btp' =>$gbr['file_name'],
                  'id' => $this->session->userdata('no_peserta'),
                );
 
                $this->mwisuda->get_update($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div><div class=\"alert alert-success alert-dismissible\" role=\"alert\"><span class=\"glyphicon glyphicon-ok-sign\" aria-hidden=\"true\"></span>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Upload berkas berhasil</div></div>");
				redirect('dashboard/wisuda');
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div><div class=\"alert alert-danger alert-dismissible\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Gagal upload berkas</div></div>");
				redirect('dashboard/wisuda');
            }
        }else{
			$this->session->set_flashdata("pesan", "<div><div class=\"alert alert-danger alert-dismissible\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Gagal upload berkas</div></div>");
				redirect('dashboard/wisuda');
		}
	}
	
	public function upload_isma(){
        $this->load->library('upload');
        $nmfile = "file_".time(); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './uploads/wisuda/'; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg|bmp|pdf'; //type yang dapat diakses bisa anda sesuaikan gif|jpg|png|jpeg|bmp
        $config['max_size'] = '10000'; //maksimum besar file 2M
        $config['file_name'] = $nmfile; //nama yang terupload nantinya
 
        $this->upload->initialize($config);
         
        if($_FILES['file_isma']['name'])
        {
            if ($this->upload->do_upload('file_isma'))
            {
                $gbr = $this->upload->data();
                $data = array(
                  'upload_isma' =>$gbr['file_name'],
                  'id' => $this->session->userdata('no_peserta'),
                );
 
                $this->mwisuda->get_update($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div><div class=\"alert alert-success alert-dismissible\" role=\"alert\"><span class=\"glyphicon glyphicon-ok-sign\" aria-hidden=\"true\"></span>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Upload berkas berhasil</div></div>");
				redirect('dashboard/wisuda');
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div><div class=\"alert alert-danger alert-dismissible\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Gagal upload berkas</div></div>");
				redirect('dashboard/wisuda');
            }
        }else{
			$this->session->set_flashdata("pesan", "<div><div class=\"alert alert-danger alert-dismissible\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Gagal upload berkas</div></div>");
				redirect('dashboard/wisuda');
		}
	}
	
	public function upload_pf(){
        $this->load->library('upload');
        $nmfile = "file_".time(); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './uploads/wisuda/'; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg|bmp|pdf'; //type yang dapat diakses bisa anda sesuaikan gif|jpg|png|jpeg|bmp
        $config['max_size'] = '10000'; //maksimum besar file 2M
        $config['file_name'] = $nmfile; //nama yang terupload nantinya
 
        $this->upload->initialize($config);
         
        if($_FILES['file_pf']['name'])
        {
            if ($this->upload->do_upload('file_pf'))
            {
                $gbr = $this->upload->data();
                $data = array(
                  'upload_pf' =>$gbr['file_name'],
                  'id' => $this->session->userdata('no_peserta'),
                );
 
                $this->mwisuda->get_update($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div><div class=\"alert alert-success alert-dismissible\" role=\"alert\"><span class=\"glyphicon glyphicon-ok-sign\" aria-hidden=\"true\"></span>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Upload berkas berhasil</div></div>");
				redirect('dashboard/wisuda');
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div><div class=\"alert alert-danger alert-dismissible\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Gagal upload berkas</div></div>");
				redirect('dashboard/wisuda');
            }
        }else{
			$this->session->set_flashdata("pesan", "<div><div class=\"alert alert-danger alert-dismissible\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Gagal upload berkas</div></div>");
				redirect('dashboard/wisuda');
		}
	}
	
	/*public function cek_1()
    {
        $btp = $this->input->post('agama');
        if (($btp != '') || (! empty($btp))) {
            return true;
        } else {
            $this->form_validation->set_message('cek_1', "Agama harus diisi.");
            return false;
        }
    }
	
	public function cek_2()
    {
        $isma = $this->input->post('agama');
        if (($isma != '') || (! empty($isma))) {
            return true;
        } else {
            $this->form_validation->set_message('cek_2', "Agama harus diisi.");
            return false;
        }
    }
	
	public function cek_3()
    {
        $fp = $this->input->post('agama');
        if (($fp != '') || (! empty($fp))) {
            return true;
        } else {
            $this->form_validation->set_message('cek_3', "Agama harus diisi.");
            return false;
        }
    }*/
}
 
/* End of file upload.php */
/* Location: ./application/controllers/upload.php */