<?php 
class Alumni extends Dashboard_Controller
{
  
	public function __construct() {
        parent::__construct();
		$this->load->model('alumni_model'); //load model mwisuda yang berada di folder model
        $this->load->helper(array('url')); //load helper url
		$this->data = array(
			'halaman' => 'Alumni',
			'main_view' => 'alumni_list',
			'title' => 'Alumni lah',
			'user' => $this->alumni_model->get_alumni()->result(),
		);
	}
	
    public function index($offset = 0)
    {
			// Untuk fungsi word_limiter().
			$this->load->helper('text');
			$alumni = $this->alumni->sort('id', 'desc')->get_all_paged($offset);
			if ($alumni) {
            $this->data['alumni'] = $alumni;
            $this->data['paging'] = $this->alumni->paging('biasa', site_url('alumni/halaman/'), 3);
			} else {
            $this->data['alumni'] = 'Tidak ada data alumni.';
			}
			$this->load->view($this->layout, $this->data);
		
    }

    public function detail($slug = '')
    {
        //$this->data['main_view'] = 'alumni_detail';
        $this->load->view($this->layout, $this->data);
	     
    }
}