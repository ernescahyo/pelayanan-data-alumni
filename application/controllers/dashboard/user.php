<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    public function index() {
        $this->load->model('user_model');
//        load library pagination
        $this->load->library('pagination');
        
//        configurasi pagination
        $config['base_url'] = base_url().'dashboard/user/#';
        $config['total_rows'] = $this->user_model->total_record();
        $config['per_page'] = 5;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config); 
        
//        menentukan offset record dari uri segment
        $start = $this->uri->segment(3, 0);
//        ubah data menjadi tampilan per limit
        $rows = $this->user_model->user_limit($config['per_page'],$start)->result();

        $data = array(
            'title' => 'harviacode.com',
            'judul' => 'Halaman User',
            'content' => 'user',
            'rows' => $rows,
            'pagination' => $this->pagination->create_links(),
            'start' => $start
        );
//      echo 'ini adalah controller user';
        $this->load->view('dashboard/user_v', $data);
    }

    public function tambah() {
        $this->load->library('form_validation');

        $data = array(
            'title' => 'harviacode.com',
            'action' => base_url() . 'index.php/user/aksitambah',
            'content' => 'user_form',
            'username' => set_value('username', ''),
            'password' => set_value('password', ''),
            'id_user' => set_value('id_user', ''),
            'tombol' => 'Tambah'
        );

        $this->load->view('layout', $data);
    }

    public function aksitambah() {

//        load library form validation
        $this->load->library('form_validation');
//        jika anda mau, anda bisa mengatur tampilan pesan error dengan menambahkan element dan css nya
        $this->form_validation->set_error_delimiters('<div style="color:red; margin-bottom: 5px">', '</div>');

//        rules validasi
        $this->form_validation->set_rules('username', 'Username', 'required|min_length[3]|max_length[20]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[3]|max_length[20]');

        if ($this->form_validation->run() == FALSE) {
//            jika validasi gagal
            $this->tambah();
        } else {
//            jika validasi berhasil
            $data = array(
                'username' => $this->input->post('username'),
                'password' => md5($this->input->post('password'))
            );

            $this->load->model('user_model');
            $this->user_model->tambah($data);

            redirect(base_url() . 'index.php/user');
        }
    }

    public function ubah($id) {

        $this->load->model('user_model');
        $row = $this->user_model->getById($id)->row();

        $data = array(
            'title' => 'harviacode.com',
            'action' => base_url() . 'index.php/user/aksiubah',
            'content' => 'user_form',
            'username' => $row->username,
            'password' => '',
            'id_user' => $row->id_user,
            'tombol' => 'Ubah'
        );

        $this->load->view('layout', $data);
    }

    public function aksiubah() {

//            warning : aksi ini tanpa ada validasi form
        $updatepassword = array(
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('password'))
        );

        $tidakupdatepassword = array(
            'username' => $this->input->post('username'),
        );

        $data = trim($this->input->post('password')) <> '' ? $updatepassword : $tidakupdatepassword;

        $this->load->model('user_model');
        $this->user_model->ubah($this->input->post('id_user'), $data);

        redirect(base_url() . 'index.php/user');
    }

    public function delete($id) {

        $this->load->model('user_model');
        $this->user_model->hapus($id);

        redirect(base_url() . 'index.php/user');
    }

}

/* End of file user.php */
/* Location: ./application/controllers/user.php */