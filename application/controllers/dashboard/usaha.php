<?php
class Usaha extends Public_Controller
{
	public $data = array(
        'halaman' => 'usaha',
        'main_view' => 'dashboard/usahav',
    );
	
	public function index() 
	{
		   $usaha = $this->input->post(null, true);

        // Validasi.
        if ($this->usaha->validate('form_rules')) {
            if ($this->usaha->simpan($usaha)) {
				
                $this->session->set_flashdata('pesanyudisium', 'Prayudisium berhasil disimpan. Kembali ke halaman ' . anchor('dashboard/home', 'home.', 'class="alert-link"'));
                redirect('dashboard/bekerja');
            } else {
                $this->session->set_flashdata('pesan_error', 'Maaf, penyimpanan Prayudisium gagal. Coba ' . anchor('dashboard/biodata', 'ulangi', 'class="alert-link"') .' beberapa saat lagi.');
                redirect('dashboard/prayudisium-error');
            }
        }
		
	        if (!$_POST) {
            $id = intval($this->session->userdata('no_peserta'));
            $this->data['values'] = (object) $this->usaha->get($id);
        } else {
            $this->data['values'] = (object) $usaha;
        }
        $this->data['form_action'] = site_url('dashboard/usaha');
        $this->load->view($this->layout, $this->data);	
	}
	
}

?>