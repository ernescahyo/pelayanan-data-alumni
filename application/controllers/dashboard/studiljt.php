<?php
class Studiljt extends Public_Controller
{
	public $data = array(
        'halaman' => 'studiljt',
        'main_view' => 'dashboard/studiljtv',
    );
	
	public function index() 
	{
		   $studi_lanjut = $this->input->post(null, true);

        // Validasi.
        if ($this->studiljt->validate('form_rules')) {
            if ($this->studiljt->simpan($studi_lanjut)) {
				
                $this->session->set_flashdata('pesanyudisium', 'Prayudisium berhasil disimpan. Kembali ke halaman ' . anchor('dashboard/home', 'home.', 'class="alert-link"'));
                redirect('dashboard/bekerja');
            } else {
                $this->session->set_flashdata('pesan_error', 'Maaf, penyimpanan Prayudisium gagal. Coba ' . anchor('dashboard/biodata', 'ulangi', 'class="alert-link"') .' beberapa saat lagi.');
                redirect('dashboard/prayudisium-error');
            }
        }
		
	        if (!$_POST) {
            $id = intval($this->session->userdata('no_peserta'));
            $this->data['values'] = (object) $this->studiljt->get($id);
        } else {
            $this->data['values'] = (object) $studi_lanjut;
        }
        $this->data['form_action'] = site_url('dashboard/studiljt');
        $this->load->view($this->layout, $this->data);	
	}
	
}

?>