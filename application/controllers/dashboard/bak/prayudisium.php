<?php
class Prayudisium extends Dashboard_Controller
{
    public $data = array(
        'halaman' => 'prayudisium',
        'main_view' => 'dashboard/prayudisium_form',
    );

    public function index()
    {
		 $prayudisium = $this->input->post(null, true);

        // Validasi.
        if ($this->prayudisium->validate('form_rules')) {
            if ($this->prayudisium->simpan($prayudisium)) {
				
                $this->session->set_flashdata('pesanyudisium', 'Prayudisium berhasil disimpan. Kembali ke halaman ' . anchor('dashboard/home', 'home.', 'class="alert-link"'));
				redirect('dashboard/prayudisium/upload');
            } else {
                $this->session->set_flashdata('pesan_error', 'Maaf, penyimpanan Prayudisium gagal. Coba ' . anchor('dashboard/biodata', 'ulangi', 'class="alert-link"') .' beberapa saat lagi.');
                redirect('dashboard/prayudisium-error');
            }
        }
		
	        if (!$_POST) {
            $id = intval($this->session->userdata('no_peserta'));
            $this->data['values'] = (object) $this->prayudisium->get($id);
        } else {
            $this->data['values'] = (object) $prayudisium;
        }
        $this->data['form_action'] = site_url('dashboard/prayudisium');
        $this->load->view($this->layout, $this->data);
    }
	
	public function sukses()
    {
        $this->data['main_view'] = 'sukses_pra';
        $this->data['title'] = 'Prayudisium Sukses';
        $this->load->view($this->layout, $this->data);
    }

    public function error()
    {
        $this->data['main_view'] = 'error';
        $this->data['title'] = 'Prayudisium Error';
        $this->load->view($this->layout, $this->data);
    }
}