<?php 
class Alumni extends Dashboard_Controller
{
    public $data = array(
        'halaman' => 'Alumni',
        'main_view' => 'alumni_list',
        'title' => 'Alumni lah',
    );

    public function index($offset = 0)
    {
// Untuk fungsi word_limiter().
        $this->load->helper('text');

        $alumni = $this->alumni->sort('id', 'desc')->get_all_paged($offset);
        if ($alumni) {
            $this->data['alumni'] = $alumni;
            $this->data['paging'] = $this->alumni->paging('biasa', site_url('alumni/halaman/'), 3);
        } else {
            $this->data['alumni'] = 'Tidak ada data alumni.';
        }
        $this->load->view($this->layout, $this->data);
    }

    public function detail($slug = '')
    {
        $alumni = $this->alumni->get('slug', $slug);
        if ($alumni) {
            $this->data['alumni'] = $alumni;
        } else {
            $this->data['alumni'] = 'alumni dengan judul yang dimaksud tidak ada.'. anchor('dashboard/alumni', ' Kembali ke halaman alumni.', 'class="alert-link"');
        }
        //$this->data['main_view'] = 'alumni_detail';
        $this->load->view($this->layout, $this->data);
    }
}