<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Upload extends Dashboard_Controller
{		
    var $limit=10;
    var $offset=10;
	var $table = 'tb_mahasiswa';
	
	public $data = array(
        'halaman' => 'upload',
        'main_view' => 'dashboard/fupload',
		'pesan_bt' => 'aaa',
    );
 
	public function view()
	{
		$this->mupload->get_data();
	}
 
    public function __construct() {
        parent::__construct();
        $this->load->model('mupload'); //load model mupload yang berada di folder model
        $this->load->helper(array('url')); //load helper url
    }
	
    public function index($page=NULL,$offset='',$key=NULL)
    {		
		$this->load->library('session');
		
        $data['titel']='Upload Berkas'; //varibel title
		
		$this->load->view($this->layout, $this->data);
	
	}
    public function add() {
         
        $data['titel']='Form Upload Berkas'; //varibel title
         
        //view yang tampil jika fungsi add diakses pada url
        $this->load->view('dashboard/prayudisium/upload',$data);
        
    }
	
    public function update_bt(){
        $this->load->library('upload');
        $nmfile = "file_".time(); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './uploads/prayudisium/'; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg|bmp|pdf'; //type yang dapat diakses bisa anda sesuaikan gif|jpg|png|jpeg|bmp
        $config['max_size'] = '10000'; //maksimum besar file 2M
        $config['file_name'] = $nmfile; //nama yang terupload nantinya
 
        $this->upload->initialize($config);
         
        if($_FILES['file_bt']['name'])
        {
            if ($this->upload->do_upload('file_bt'))
            {
                $gbr = $this->upload->data();
                $data = array(
                  'upload_bebas_tanggungan' =>$gbr['file_name'],
                  'id' => $this->session->userdata('no_peserta'),
                );
 
                $this->mupload->get_update($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div><div class=\"alert alert-success alert-dismissible\" role=\"alert\"><span class=\"glyphicon glyphicon-ok-sign\" aria-hidden=\"true\"></span>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Upload berkas berhasil</div></div>");
				redirect('dashboard/prayudisium/upload');
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div><div class=\"alert alert-danger alert-dismissible\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Gagal upload berkas</div></div>");
				redirect('dashboard/prayudisium/upload');
            }
        }
	}
		
	public function update_ukt(){
        $this->load->library('upload');
        $nmfile = "file_".time(); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './uploads/'; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg|bmp|pdf'; //type yang dapat diakses bisa anda sesuaikan gif|jpg|png|jpeg|bmp
        $config['max_size'] = '10000'; //maksimum besar file 2M
        $config['file_name'] = $nmfile; //nama yang terupload nantinya
 
        $this->upload->initialize($config);
         
        if($_FILES['file_ukt']['name'])
        {
            if ($this->upload->do_upload('file_ukt'))
            {
                $gbr = $this->upload->data();
                $data = array(
                  'upload_ukt' =>$gbr['file_name'],
                  'id' => $this->session->userdata('no_peserta'),
                );
 
                $this->mupload->get_update($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div><div class=\"alert alert-success alert-dismissible\" role=\"alert\"><span class=\"glyphicon glyphicon-ok-sign\" aria-hidden=\"true\"></span>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Upload berkas berhasil</div></div>");
				redirect('dashboard/prayudisium/upload');
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
               $this->session->set_flashdata("pesan", "<div><div class=\"alert alert-danger alert-dismissible\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Gagal upload berkas</div></div>");
				redirect('dashboard/prayudisium/upload');
            }
        }
    }	
		
	public function update_bta(){
        $this->load->library('upload');
        $nmfile = "file_".time(); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './uploads/'; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg|bmp|pdf'; //type yang dapat diakses bisa anda sesuaikan gif|jpg|png|jpeg|bmp
        $config['max_size'] = '10000'; //maksimum besar file 2M
        $config['file_name'] = $nmfile; //nama yang terupload nantinya
 
        $this->upload->initialize($config);
         
        if($_FILES['file_bta']['name'])
        {
            if ($this->upload->do_upload('file_bta'))
            {
                $gbr = $this->upload->data();
                $data = array(
                  'Upload_bta' =>$gbr['file_name'],
                  'id' => $this->session->userdata('no_peserta'),
                );
 
                $this->mupload->get_update($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div><div class=\"alert alert-success alert-dismissible\" role=\"alert\"><span class=\"glyphicon glyphicon-ok-sign\" aria-hidden=\"true\"></span>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Upload berkas berhasil</div></div>");
				redirect('dashboard/prayudisium/upload');
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div><div class=\"alert alert-danger alert-dismissible\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Gagal upload berkas</div></div>");
				redirect('dashboard/prayudisium/upload');
            }
        }
	}		

	public function update_bpkl(){
        $this->load->library('upload');
        $nmfile = "file_".time(); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './uploads/'; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg|bmp|pdf'; //type yang dapat diakses bisa anda sesuaikan gif|jpg|png|jpeg|bmp
        $config['max_size'] = '10000'; //maksimum besar file 2M
        $config['file_name'] = $nmfile; //nama yang terupload nantinya
 
        $this->upload->initialize($config);
         
        if($_FILES['file_bpkl']['name'])
        {
            if ($this->upload->do_upload('file_bpkl'))
            {
                $gbr = $this->upload->data();
                $data = array(
                  'upload_bpkl' =>$gbr['file_name'],
                  'id' => $this->session->userdata('no_peserta'),
                );
 
                $this->mupload->get_update($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div><div class=\"alert alert-success alert-dismissible\" role=\"alert\"><span class=\"glyphicon glyphicon-ok-sign\" aria-hidden=\"true\"></span>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Upload berkas berhasil</div></div>");
				redirect('dashboard/prayudisium/upload');
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div><div class=\"alert alert-danger alert-dismissible\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Gagal upload berkas</div></div>");
				redirect('dashboard/prayudisium/upload');
            }
        }
	}		

	public function update_lta(){
        $this->load->library('upload');
        $nmfile = "file_".time(); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './uploads/'; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg|bmp|pdf'; //type yang dapat diakses bisa anda sesuaikan gif|jpg|png|jpeg|bmp
        $config['max_size'] = '10000'; //maksimum besar file 2M
        $config['file_name'] = $nmfile; //nama yang terupload nantinya
 
        $this->upload->initialize($config);
         
        if($_FILES['file_lta']['name'])
        {
            if ($this->upload->do_upload('file_lta'))
            {
                $gbr = $this->upload->data();
                $data = array(
                  'upload_ta' =>$gbr['file_name'],
                  'id' => $this->session->userdata('no_peserta'),
                );
 
                $this->mupload->get_update($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div><div class=\"alert alert-success alert-dismissible\" role=\"alert\"><span class=\"glyphicon glyphicon-ok-sign\" aria-hidden=\"true\"></span>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Upload berkas berhasil</div></div>");
				redirect('dashboard/prayudisium/upload');
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div><div class=\"alert alert-danger alert-dismissible\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Gagal upload berkas</div></div>");
				redirect('dashboard/prayudisium/upload');
            }
        }
	}		
	
	public function update_lpkl(){
        $this->load->library('upload');
        $nmfile = "file_".time(); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './uploads/'; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg|bmp|pdf'; //type yang dapat diakses bisa anda sesuaikan gif|jpg|png|jpeg|bmp
        $config['max_size'] = '10000'; //maksimum besar file 2M
        $config['file_name'] = $nmfile; //nama yang terupload nantinya
 
        $this->upload->initialize($config);
         
        if($_FILES['file_lpkl']['name'])
        {
            if ($this->upload->do_upload('file_lpkl'))
            {
                $gbr = $this->upload->data();
                $data = array(
                  'upload_pkl' =>$gbr['file_name'],
                  'id' => $this->session->userdata('no_peserta'),
                );
 
                $this->mupload->get_update($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div><div class=\"alert alert-success alert-dismissible\" role=\"alert\"><span class=\"glyphicon glyphicon-ok-sign\" aria-hidden=\"true\"></span>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Upload berkas berhasil</div></div>");
				redirect('dashboard/prayudisium/upload');
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div><div class=\"alert alert-danger alert-dismissible\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Gagal upload berkas</div></div>");
				redirect('dashboard/prayudisium/upload');
            }
        }
	}		

	public function update_ns(){
        $this->load->library('upload');
        $nmfile = "file_".time(); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './uploads/'; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg|bmp|pdf'; //type yang dapat diakses bisa anda sesuaikan gif|jpg|png|jpeg|bmp
        $config['max_size'] = '10000'; //maksimum besar file 2M
        $config['file_name'] = $nmfile; //nama yang terupload nantinya
 
        $this->upload->initialize($config);
         
        if($_FILES['file_ns']['name'])
        {
            if ($this->upload->do_upload('file_ns'))
            {
                $gbr = $this->upload->data();
                $data = array(
                  'upload_sementara' =>$gbr['file_name'],
                  'id' => $this->session->userdata('no_peserta'),
                );
 
                $this->mupload->get_update($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div><div class=\"alert alert-success alert-dismissible\" role=\"alert\"><span class=\"glyphicon glyphicon-ok-sign\" aria-hidden=\"true\"></span>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Upload berkas berhasil</div></div>");
				redirect('dashboard/prayudisium/upload');
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div><div class=\"alert alert-danger alert-dismissible\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Gagal upload berkas</div></div>");
				redirect('dashboard/prayudisium/upload');
            }
        }
	}		
}
 
/* End of file upload.php */
/* Location: ./application/controllers/upload.php */