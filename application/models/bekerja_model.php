<?php
class Bekerja_model extends MY_Model
{
    protected $_tabel = 'tb_mahasiswa';
   
    public $form_rules = array(
        // Data Pribadi ----------------------------------------------------
        array(
            'field' => 'nama_perusahaan',
            'label' => 'nama_perusahaan',
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
		array( /* dipakai untuk aturan untuk isian field harus diisi akan tampil validation error atau true*/
            'field' => 'website_perusahaan',
            'label' => 'Website Perusahaan',
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
		array( /* dipakai untuk aturan untuk isian field harus diisi akan tampil validation error atau true*/
            'field' => 'instansi_tempat_kerja',
            'label' => 'Instansi Tempat Kerja',/* label buat tampil saat notif keluar */
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
		array( /* dipakai untuk aturan untuk isian field harus diisi akan tampil validation error atau true*/
            'field' => 'tahun_masuk_kerja',
            'label' => 'Tahun Masuk Kerja',/* label buat tampil saat notif keluar */
            'rules' => 'trim|xss_clean|required|max_length[4]'
        ),
		array( /* dipakai untuk aturan untuk isian field harus diisi akan tampil validation error atau true*/
            'field' => 'posisi_jabatan_saat_ini',
            'label' => 'Posisi Jabatan Anda saat ini',/* label buat tampil saat notif keluar */
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
	
	
		
    );
	
	     

    public function simpan($peserta)
    {
        $peserta = (object)$peserta;
		

        // Set status biodata.
        $peserta->status_alumni = 'Bekerja';
        // Set status biodata.

        return $this->update($peserta->id, $peserta);
    }
}