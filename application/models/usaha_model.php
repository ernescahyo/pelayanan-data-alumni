<?php
class usaha_model extends MY_Model
{
    protected $_tabel = 'tb_mahasiswa';
   
    public $form_rules = array(
        // Data Pribadi ----------------------------------------------------
        array(
            'field' => 'nama_usaha',
            'label' => 'Nama Perusahaan',
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
		array( /* dipakai untuk aturan untuk isian field harus diisi akan tampil validation error atau true*/
            'field' => 'alamat_usaha',
            'label' => 'Alamat Perusahaan',
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
		array( /* dipakai untuk aturan untuk isian field harus diisi akan tampil validation error atau true*/
            'field' => 'website_usaha',
            'label' => 'Website Perusahaan',/* label buat tampil saat notif keluar */
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
		array( /* dipakai untuk aturan untuk isian field harus diisi akan tampil validation error atau true*/
            'field' => 'bidang_usaha',
            'label' => 'Bidang Usaha',/* label buat tampil saat notif keluar */
            'rules' => 'trim|xss_clean|required|max_length[30]'
        ),
		array( /* dipakai untuk aturan untuk isian field harus diisi akan tampil validation error atau true*/
            'field' => 'tahun_berdiri',
            'label' => 'tahun Berdiri',/* label buat tampil saat notif keluar */
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
	
	
	
		
    );
	
	     

    public function simpan($peserta)
    {
        $peserta = (object)$peserta;
		

        // Set status biodata.
        $peserta->status_alumni = '2';
        // Set status biodata.

        return $this->update($peserta->id, $peserta);
    }
}