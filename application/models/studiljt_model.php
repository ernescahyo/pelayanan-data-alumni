<?php
class Studiljt_model extends MY_Model
{
    protected $_tabel = 'tb_mahasiswa';
   
    public $form_rules = array(
        // Data Pribadi ----------------------------------------------------
        array(
            'field' => 'nama_pt',
            'label' => 'Nama Perguruan Tinggi',
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
		array( /* dipakai untuk aturan untuk isian field harus diisi akan tampil validation error atau true*/
            'field' => 'kota',
            'label' => 'Kota',
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
		array( /* dipakai untuk aturan untuk isian field harus diisi akan tampil validation error atau true*/
            'field' => 'negara',
            'label' => 'Negara',/* label buat tampil saat notif keluar */
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
		array( /* dipakai untuk aturan untuk isian field harus diisi akan tampil validation error atau true*/
            'field' => 'j_pendidikan',
            'label' => 'Jenis Pendidikan',/* label buat tampil saat notif keluar */
            'rules' => 'trim|xss_clean|required|max_length[30]'
        ),
		array( /* dipakai untuk aturan untuk isian field harus diisi akan tampil validation error atau true*/
            'field' => 'bidang_studi',
            'label' => 'Bidang Studi',/* label buat tampil saat notif keluar */
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
		array( /* dipakai untuk aturan untuk isian field harus diisi akan tampil validation error atau true*/
            'field' => 'biaya',
            'label' => 'Biaya',/* label buat tampil saat notif keluar */
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
	
	
		
    );
	
	     

    public function simpan($peserta)
    {
        $peserta = (object)$peserta;
		

        // Set status biodata.
        $peserta->status_alumni = '1';
        // Set status biodata.

        return $this->update($peserta->id, $peserta);
    }
}