<?php
class Alumni_model extends MY_Model
{
    protected $_per_page = 100;
    protected $_tabel = 'tb_mahasiswa';

    protected $form_rules = array(
        array(
            'field' => 'nama',
            'label' => 'Nama',
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
        array(
            'field' => 'status_alumni',
            'label' => 'Status',
            'rules' => 'trim|xss_clean|required'
        ),
    );

    public $default_value = array(
        'judul' => '',
        'isi' => '',
    );

    public function tambah($pengumuman)
    {
        $pengumuman = (object) $pengumuman;        
		$pengumuman->slug = url_title($pengumuman->judul, '-', TRUE);
        return $this->insert($pengumuman);
    }

	public function get_alumni()
	{
		$this->db->where('status_akademik', '1');
		return $this->db->get('tb_mahasiswa');		
	}
	
    public function edit($id, $pengumuman)
    {
        $pengumuman = (object) $pengumuman;        
		$pengumuman->slug = url_title($pengumuman->judul, '-', TRUE);
        return $this->update($id, $pengumuman);
    }
  
}