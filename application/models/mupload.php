<?php
class Mupload extends CI_Model {
    var $tabel = 'tb_mahasiswa'; //buat variabel tabel
 
    function __construct() {
        parent::__construct();
    }
	
	public function get_data($values)
	{	
		$this->db->select('upload_bebas_tanggungan, upload_ukt, upload_bta, upload_bpkl, upload_ta, upload_pkl, upload_sementara');
		$this->db->where('id', $this->session->userdata('no_peserta'));
		return $this->db->get($this->tabel);
	}
 
    //fungsi insert ke database
    function get_update($data){
		$this->db->where('id', $data['id']);
		return $this->db->update($this->tabel, $data);
    }
}
?>