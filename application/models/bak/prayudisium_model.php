<?php
class Prayudisium_model extends MY_Model
{
    protected $_tabel = 'tb_mahasiswa';
   
    public $form_rules = array(
        // Data Pribadi ----------------------------------------------------
        array(
            'field' => 'nama',
            'label' => 'Nama',
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
		 array(
            'field' => 'angkatan',
            'label' => 'Angkatan',
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
		 array(
            'field' => 'ip_1',
            'label' => 'IP Semester 1',
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
		 array(
            'field' => 'ip_2',
            'label' => 'IP Semester 2',
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
		 array(
            'field' => 'ip_3',
            'label' => 'IP Semester 3',
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
		 array(
            'field' => 'ip_4',
            'label' => 'IP Semester 4',
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
		 array(
            'field' => 'ip_5',
            'label' => 'IP Semester 5',
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
		 array(
            'field' => 'ip_6',
            'label' => 'IP Semester 6',
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
		 array(
            'field' => 'nilai_TA',
            'label' => 'Nilai TA',
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
		 array(
            'field' => 'judul_PKL',
            'label' => 'Judul PKL',
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
		 array(
            'field' => 'nilai_PKL',
            'label' => 'Nilai PKL',
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
		
    );
	
	     

    public function simpan($peserta)
    {
        $peserta = (object)$peserta;

        // Set status biodata.
        $peserta->status_biodata = '1';

        // Ubah tanggal lahir ke format yyyy-mm-dd
        /*$peserta->tanggal_lahir = date_to_en($peserta->tanggal_lahir);
		$peserta->tanggal_lulus = date_to_en($peserta->tanggal_lulus);*/
        
      
        return $this->update($peserta->id, $peserta);
    }
}