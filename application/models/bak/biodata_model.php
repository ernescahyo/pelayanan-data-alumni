<?php
class Biodata_model extends MY_Model
{
    protected $_tabel = 'tb_mahasiswa';

    public $form_rules = array(
        // Data Pribadi ----------------------------------------------------
        array(
            'field' => 'nama',
            'label' => 'Nama',
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
        array(
            'field' => 'jenis_kelamin',
            'label' => 'Jenis Kelamin',
            'rules' => 'trim|xss_clean|required'
        ),
		 array(
            'field' => 'tempat_lahir',
            'label' => 'Tempat Lahir',
            'rules' => 'trim|xss_clean|required|max_length[32]'
        ),
        array(
            'field' => 'tanggal_lahir',
            'label' => 'Tanggal Lahir',
            'rules' => 'trim|xss_clean|required|max_length[10]|callback__format_tanggal'
        ),
			array(
            'field' => 'status_perkawinan',
            'label' => 'Status Perkawinan',
            'rules' => 'trim|xss_clean|required'
        ),
		 array(
            'field' => 'berat_badan',
            'label' => 'Berat Badan',
            'rules' => 'trim|xss_clean|required|numeric|max_length[3]|greater_than[0]'
        ),
		   array(
            'field' => 'tinggi_badan',
            'label' => 'Tinggi Badan',
            'rules' => 'trim|xss_clean|required|numeric|max_length[3]|greater_than[0]'
        ),
        
	
		  array(
            'field' => 'agama',
            'label' => 'Agama',
            'rules' => 'trim|xss_clean|required|callback__cek_agama'
        ),
       
      
		  array(
            'field' => 'tmp_alamat',
            'label' => 'Alamat Tinggal',
            'rules' => 'trim|xss_clean|required|max_length[255]'
        ),
		    array(
            'field' => 'tmp_telepon',
            'label' => 'Telepon Tempat Tinggal',
            'rules' => 'trim|xss_clean|required|max_length[16]'
        ),
        
       
        // Orang tua -------------------------------------------------------
        array(
            'field' => 'ort_nama_ayah',
            'label' => 'Nama Ayah',
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
		
      array(
            'field' => 'tanggal_lulus',
            'label' => 'Tanggal Lulus',
            'rules' => 'trim|xss_clean|required|max_length[10]|callback__format_tanggal'
        ),
       
		array(
            'field' => 'lama_studi',
            'label' => 'Lama Studi',
            'rules' => 'trim|xss_clean|required|max_length[3]|greater_than[0]'
		),
		array(
            'field' => 'ipk',
            'label' => 'Indeks Prestasi Kumulatif',
            'rules' => 'trim|xss_clean|required|max_length[5]|greater_than[0]'
		),
		array(
            'field' => 'lama_lap',
            'label' => 'Lama Penyusunan Laporan',
            'rules' => 'trim|xss_clean|required|max_length[3]|greater_than[0]'
		),
		array(
            'field' => 'judul_ta',
            'label' => 'Judul Laporan TA',
            'rules' => 'trim|xss_clean|required|max_length[100]|greater_than[0]'
        ),
    );

    public function simpan($peserta)
    {
        $peserta = (object)$peserta;

        // Set status biodata.
        $peserta->status_biodata = '1';

        // Ubah tanggal lahir ke format yyyy-mm-dd
        $peserta->tanggal_lahir = date_to_en($peserta->tanggal_lahir);
		$peserta->tanggal_lulus = date_to_en($peserta->tanggal_lulus);
        
       
        return $this->update($peserta->id, $peserta);
    }
}