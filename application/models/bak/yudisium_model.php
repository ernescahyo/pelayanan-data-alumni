<?php
class Yudisium_model extends MY_Model
{
    protected $_tabel = 'tb_mahasiswa';

    public $form_rules = array(
        // Data Pribadi ----------------------------------------------------
        array(
            'field' => 'nama',
            'label' => 'Nama',
            'rules' => 'trim|xss_clean|required|max_length[64]'
        ),
    );

    public function simpan($peserta)
    {
        $peserta = (object)$peserta;

        // Set status biodata.
        $peserta->status_biodata = '1';

        // Ubah tanggal lahir ke format yyyy-mm-dd
        $peserta->tanggal_lahir = date_to_en($peserta->tanggal_lahir);
		$peserta->tanggal_lulus = date_to_en($peserta->tanggal_lulus);
        
       
        return $this->update($peserta->id, $peserta);
    }
}