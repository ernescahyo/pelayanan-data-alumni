<?php
class Mwisuda extends CI_Model {
    var $tabel = 'tb_mahasiswa'; //buat variabel tabel
 
    function __construct() {
        parent::__construct();
    }
	
	public function get_data($values)
	{	
		$this->db->select('upload_btp, upload_isma, upload_pf');
		$this->db->where('id', $this->session->userdata('no_peserta'));
		return $this->db->get($this->tabel);
	}
	
	function get_update($data){
		$this->db->where('id', $data['id']);
		return $this->db->update($this->tabel, $data);
    }
}
?>