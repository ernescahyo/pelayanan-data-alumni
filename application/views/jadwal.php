  <h3><?php echo $title ?></h3>
  <hr class="hr-primary">
    <table class="table table-striped table-bordered table-hover table-condensed" style="max-width: 500px">
        <thead>
        <tr>
            <th>Tanggal</th>
            <th>Kegiatan</th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td>September 2017</td>
            <td>Pendaftaran Data Alumni</td>
        </tr>
        <tr>
            <td>Sebelum 7 Oktober 2017</td>
            <td>Verifikasi Data Alumni I</td>
        </tr>
		 <tr>
            <td>15 s.d 18 Oktober 2017</td>
            <td>Verifikasi Data Alumni II</td>
        </tr>
        </tbody>
    </table>
