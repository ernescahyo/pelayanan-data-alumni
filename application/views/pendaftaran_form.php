    <h3>Daftar</h3>
	<hr class="hr-primary">
	<div class="alert alert-danger">
<p><strong>Catatan:</strong></br>
Email harus diisi alamat email yang valid dan aktif, karena akan digunakan untuk mengirim informasi.</li>
</p></div>

<div class="panel panel-default">
  <div class="panel-body">
    <?php echo form_open('pendaftaran', array('id'=>'myform', 'class'=>'myform', 'role'=>'form', 'method'=>'post')) ?>

        <div class="form-group has-feedback <?php set_validation_style('nim')?>">
            <?php echo form_label('NIM', 'nim', array('class' => 'control-label')) ?>
            <?php echo form_input('nim', $values->nim, 'id="nim" class="form-control" placeholder="NIM" maxlength="9"') ?>
            <?php set_validation_icon('nim') ?>
            <?php echo form_error('nim', '<span class="help-block">', '</span>');?>
        </div>

		<div class="form-group has-feedback <?php set_validation_style('username')?>">
            <?php echo form_label('Username', 'username', array('class' => 'control-label')) ?>
            <?php echo form_input('username', $values->username, 'id="username" class="form-control" placeholder="Username"') ?>
            <?php set_validation_icon('username') ?>
            <?php echo form_error('username', '<span class="help-block">', '</span>');?>
        </div>
		
        <div class="form-group has-feedback <?php set_validation_style('email')?>">
            <?php echo form_label('Email', 'email', array('class' => 'control-label')) ?>
            <?php echo form_input('email', $values->email, 'id="email" class="form-control" pattern="[a-z0-9._%+-]+@stmik-aub.ac.id$" placeholder="ex: stmik-aub.ac.id" maxlength="64"') ?>
            <?php set_validation_icon('email') ?>
            <?php echo form_error('email', '<span class="help-block">', '</span>');?>
        </div>

        <div class="form-group has-feedback <?php set_validation_style('nama')?>">            
            <?php echo form_label('Nama Lengkap', 'nama', array('class' => 'control-label')) ?>
            <?php echo form_input('nama', $values->nama, 'id="nama" class="form-control" placeholder="Nama Lengkap" maxlength="64"') ?>            
            <?php set_validation_icon('nama') ?>
            <?php echo form_error('nama', '<span class="help-block">', '</span>');?>
        </div>

        <div class="form-group has-feedback <?php set_validation_style('nama_panggilan')?>">            
            <?php echo form_label('Nama Panggilan', 'nama_panggilan', array('class' => 'control-label')) ?>
            <?php echo form_input('nama_panggilan', $values->nama_panggilan, 'id="nama_panggilan" class="form-control" placeholder="Nama Panggilan" maxlength="32"') ?>            
            <?php set_validation_icon('nama_panggilan') ?>
            <?php echo form_error('nama_panggilan', '<span class="help-block">', '</span>');?>
        </div>

<div class="row clearfix">
		<div class="col-md-5 column">					
<p><?php echo $captcha; ?></p>      
		</div>
		<div class="col-md-7 column">
<div class="form-group has-feedback <?php set_validation_style('captcha')?>">            
            <?php echo form_input('captcha', $values->captcha, 'id="captcha" class="form-control input-lg" placeholder="Input Kode" maxlength="4"') ?>
            <?php set_validation_icon('captcha') ?>
            <?php echo form_error('captcha', '<span class="help-block">', '</span>');?>
        </div>		
		</div>
</div>		
<p><?php echo form_button(array('content'=>'Daftar', 'type'=>'submit', 'class'=>'btn btn-primary', 'data-confirm'=>'Anda yakin akan menyimpan data ini?')) ?>
</p>
    <?php echo form_close() ?>

	</div>
</div>	 