<h3><?php echo $title ?></h3>
<hr class="hr-primary">
    <ol>
        <li>
            <strong>Pendaftaran.</strong> Mendaftar melalui halaman berikut ini
            <?php echo anchor('pendaftaran', 'Pendaftaran'); ?>.           
        </li>
		<li><strong>Mengisi Form.</strong> Email harus diisi alamat email yang valid dan aktif, karena akan digunakan untuk mengirim informasi.</li>
        <li><strong>Login.</strong> Melakukan login dengan username dan password yang sudah diberikan.</li>
         <li><strong>Mengisi Biodata.</strong> Jika Anda belum melengkapi biodata, silakan melengkapinya. Klik tombol "Biodata"</li>
		  <li><strong>Simpan.</strong> Jangan lupa klik tombol simpan. Sewaktu-waktu update data alumni anda.</li>
    </ol>