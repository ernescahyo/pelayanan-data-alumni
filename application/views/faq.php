<h3><?php echo $title ?></h3>
<hr class="hr-primary">
   <p><strong><em>Tanya : Apakah Pelayanan Data Alumni itu?</em></strong></p>
<p>Jawab : Data alumni merupakan data akademik dan non-akademik yang kami kumpulkan selama Anda bersekolah atau berkegiatan di Perguruan Tinggi. Dengan begitu kami berharap bahwa data tersebut mendekati gambaran profil Anda dan dapat meningkatkan komunikasi kami dengan Anda.</p>
<p>&nbsp;</p>
<p><strong><em>Tanya : Untuk apa Pelayanan Data Alumni dibuat?</em></strong></p>
<p>Jawab : Data Alumni yang kami kumpulkan ditujukan untuk menjalin silaturahmi para alumni lulusan Perguruan Tinggi. Perguruan Tinggi menggunakan data Anda untuk berbagai macam komunikasi melalui pos, <em>e-mail</em>, telepon, SMS, media sosial, dan aplikasi berkirim pesan seperti Whatssapp, yang termasuk distribusi publikasi lowongan kerja dan beasiswa, <em>tracer study</em>, promosi kegiatan, undangan kegiatan, dan pengumpulan dana.</p>
<p>&nbsp;</p>
<p><strong><em>Tanya : Apakah Data Informasi saya dijamin?</em></strong></p>
<p>Jawab : Data alumni yang kami miliki digunakan terbatas untuk kegiatan-kegiatan yang diselenggarakan oleh unit-unit kerja di Perguruan Tinggi. Distribusi data tersebut diatur ketat dalam sebuah aturan sehingga distribusi data kepada unit kerja di Perguruan Tinggi yang membutuhkan terbatas pada data yang dibutuhkan untuk pelaksanaan suatu kegiatan dan bukan keseluruhan detail data Anda yang terdapat dalam basis data kami. Data Anda bersifat rahasia selamanya dan hanya dibuka kepada pihak di luar Perguruan Tinggi jika dibenarkan oleh hukum.</p>
<p>&nbsp;</p>
<p><strong><em>Tanya : Bagaimana mendaftar menjadi anggota?</em></strong></p>
<p>Jawab : Pendaftaran dapat langsung melakukan registrasi online pada website ini.</p>
