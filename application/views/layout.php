<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Pelayanan Data Alumni</title>
	<link rel="icon" href="<?php echo base_url('asset/img/favicon.png')?>">
    <link href="<?php echo base_url('asset/bootstrap_3_2_0/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/bootstrap_3_2_0/css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/hovernav/hovernav.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/bootstrap_datepicker/css/datepicker.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/css/style.css'); ?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="<?php echo base_url('asset/html5shiv/html5shiv.min.js'); ?>"></script>
    <script src="<?php echo base_url('asset/respond/respond.min.js'); ?>"></script>
    <![endif]-->
</head>
<body>
 <!-- Awal Banner -->
	   <div class="row" style="margin-bottom:5px">
        <div class="alert banner-info" style="width:100%;">
        	<div class="container">
        	<div class="pull-left">
   <img src="<?php echo base_url ('asset/img/logo.png');?>" width="72" height="72" style="padding:5px;">
          </div>
          <h3 class="alert-heading" style="color:#fff;text-shadow: 0.1em 0.1em 0.05em #333; line-height:14px;margin-top:10px;">Pelayanan Data Alumni</h3>
          <h5 class="alert-heading" style="color:#000;text-shadow: 0.1em 0.1em 0.05em #333; line-height:14px;">Perguruan Tinggi</h5>
        </div>
        </div>
  </div>
	<!-- Akhir Banner -->
	<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
	 <?php
        $login_status = $this->session->userdata('login_status');
        $user_level = $this->session->userdata('user_level');
        if ($login_status === true && $user_level == 'peserta') {
            $this->load->view('dashboard/navbar');
        } else {
            $this->load->view('navbar');
        }
    ?>
	

		 <?php
        $login_status = $this->session->userdata('login_status');
        $user_level = $this->session->userdata('user_level');
        if ($login_status === true && $user_level == 'peserta') {
          ?>  
		  <div class="row clearfix">
		<?php $this->load->view($main_view); ?>
		</div>
        <?php } else 
		{ ?>
	<div class="row clearfix">
		<div class="col-md-8 column">					
		 <?php $this->load->view($main_view); ?>
		</div>
		<div class="col-md-4 column">
            <!--Sidebar Pengumuman-->
			<div class="well"> 
              <h3>Pengumuman</h3>
           <div class="media">  
		
		       <?php foreach ($home as $row){?>
  <div class="media-body"> 
    <h5 class="media-heading"><?php echo $row->judul ?></a></h5>
	<p class="text-danger"><?php echo format_hari_tanggal($row->created_at) ?>
	<?php
                if ($login_status === true && $user_level === 'peserta') {
                    echo anchor(site_url('dashboard/pengumuman/'.$row->slug), 'Baca');
                } elseif ($login_status === true && $user_level === 'administrator') {
                    echo anchor(site_url('admin/pengumuman/'.$row->slug), 'Baca');
                } else {
                    echo anchor(site_url('pengumuman/'.$row->slug), 'Baca');
                }
                ?></p>
	<hr>
	</div>
    <?php } ?>
			</div>
      
          </div>
<!--end sidebar-->
		</div>  <!--end div 4-->
	</div>


  <?php } ?>		
</div>
</div>
</div>

<div class="footer">
      <div class="container">
        <p style="margin: 20px 0;">Copyright &#169; <?php echo date("Y") ?> &#124; Penelitian Dosen Pemula</p>
      </div>
    </div>

<noscript>
    <p class="noscript">Javascript pada browser Anda tidak diaktifkan. Silakan mengaktifkan Javascript.</p>
    <style type="text/css">
        #wrapper { display:none; }
        .noscript {
            text-align: center;
            color: #ff0000;
            font-size: 1.5em;
            vertical-align: 50%;
            margin: 250px;
            border: 1px solid;
        }
    </style>
</noscript>
<script src="<?php echo base_url('asset/js/jquery-1.11.1.min.js'); ?>"></script>
    <script src="<?php echo base_url('asset/bootstrap_3_2_0/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('asset/hovernav/hovernav.js'); ?>"></script>
    <script src="<?php echo base_url('asset/bootstrap_datepicker/js/bootstrap-datepicker.js'); ?>"></script>
    <script src="<?php echo base_url('asset/js/psb.js'); ?>"></script>
</body>
</html>