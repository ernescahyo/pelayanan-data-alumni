<h3>Error</h3>	
<hr class="hr-primary">

    <div class="row">
        <div class="col-md-8 column">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <span class="sr-only">Error:</span>
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                <?php foreach($pesan_error as $error): ?>
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <?php echo $error; ?>
                    <br>
                <?php endforeach ?>
            </div>
        </div>
    </div>
