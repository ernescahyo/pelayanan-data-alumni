<style>
  /* Note: Try to remove the following lines to see the effect of CSS positioning */
  .affix {
      top: 0;
      width: 100%;
	  z-index: 9999;
  }
  
  .header {
	  background: #ffffff;
	  padding: 16px;
	  height: 100px;
  }
  
  

  .affix + .container-fluid {
	  position: relative;
      top: 70px;
  }
  </style>

<nav class="navbar navbar-inverse" role="navigation" data-spy="affix" data-offset-top="98">
    <div class="container">

        <!-- Navbar Link -->
        <ul class="nav navbar-nav navbar-left">
            <!-- Dropdown Home -->
            <?php echo (isset($halaman) && $halaman == 'home') ? '<li class="active">' : '<li>'; ?> <?php echo anchor(base_url(), '<span class="glyphicon glyphicon-home"></span> Home');?></li>

        <!-- Form Login Peserta -->
        <form method="post" action=" <?php echo site_url('login'); ?>" class="navbar-form navbar-right">
            <div class="form-group">
                <label class="sr-only" for="username">Username</label>
                <input type="text" name="username" class="form-control" id="username" placeholder="username" value="<?php echo set_value('username'); ?>">
            </div>
            <div class="form-group">
                <label class="sr-only" for="password">Password</label>
                <input type="password" name="password" class="form-control" id="password" placeholder="password" value="<?php echo set_value('password'); ?>">
            </div>
            <button type="submit" class="btn btn-default">Login</button>
        </form>
        <!-- /Form Login Peserta -->

    </div> <!-- container -->
</nav>