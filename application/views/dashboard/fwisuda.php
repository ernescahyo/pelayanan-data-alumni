<head>
    <title><?=$titel?></title> <!-- variabel diambil dari controller -->
     
    <link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet"> <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>assets/css/style.css" rel="stylesheet"> <!-- Custom styles for this template -->
</head>

 <!--div class="form-group form-group-sm">        
        <!?php echo form_label('NIM', 'NIM', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-2">
            <p class="form-control-static"><!?php echo $values->nim;?></p>
        </div>
    </div>
	<div class="form-group form-group-sm">        
        <!?php echo form_label('Nama', 'Nama', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-2">
            <p class="form-control-static"><!?php echo $up_data[5];?></p>
        </div>
    </div>
	<div class="form-group form-group-sm">        
        <!?php echo form_label('IPK', 'IPK', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-2">
            <p class="form-control-static"><!?php echo $values->ipk;?></p>
        </div>
    </div-->
	
<div class="container">
      <!-- Main component for a primary marketing message or call to action -->
  <?=$this->session->flashdata('pesan')?>
  
<div class="panel panel-default">
  <div class="panel-heading"><b>Form Upload Berkas Wisuda</b></div>
  <div class="panel-body">
  
     <form action="<?=base_url()?>dashboard/wisuda/upload_btp" method="post" enctype="multipart/form-data">
       <table class="table table-striped">
	   
		 <tr>
			<h5 class="bg-success">A. File Bebas Tanggungan Perpustakaan</h5>
         </tr>
		 
		 <tr>
          <td style="width:15%;"></td>
          <td>
            <div class="col-sm-6 has-feedback <?php set_validation_style('file_btp')?>">
                <input type="file" name="file_btp" id="file_btp" accept="image/gif, image/jpeg, image/png, image/bmp, application/pdf">
				<?php 
					if(!empty($btp)){
						$status_btp = 1;
						echo "<br><strong>Status : </strong><span class='alert-success' style='padding:2px'><span class='glyphicon glyphicon-ok-sign'></span>&nbsp;<font color='#3c763d'>Anda sudah meng-upload file.</font>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='"; echo base_url('uploads/wisuda/'); echo "/"; echo $btp; echo "' target='_blank'>Preview&nbsp;</a></span>";
					}else{
						$status_btp = 0;
						echo "<br><strong>Status : </strong><span class='alert-danger' style='padding:2px'><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;<font color='#a94442'>Anda belum meng-upload file.</font></span>";
					}
				?>
            <?php set_validation_icon('file_btp') ?>
			<?php echo form_error('file_btp', '<span class="help-block">', '</span>');?>
				<br>
            </div>
			<?php if (form_error('file_btp')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('file_btp', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
          </td>
		  </tr>
		  <tr>
		  <td style="width:15%;"></td>
		  <td>
		  <div class="col-sm-6">
			<input type="submit" value="Upload" class="btn btn-default">
		  </div>
		  </td>
         </tr>
		</table>
		</form>
		
		
     <form action="<?=base_url()?>dashboard/wisuda/upload_isma" method="post" enctype="multipart/form-data">
	   <table class="table table-striped">
	   		
		 <tr>
			<h5 class="bg-success">B. File Ijazah SMA</h5>
         </tr>
		 
		 <tr>
          <td style="width:15%;"></td>
          <td>
            <div class="col-sm-6 <?php set_validation_style('isma')?>">
                <input type="file" name="file_isma" accept="image/gif, image/jpeg, image/png, image/bmp, application/pdf">
				<?php 
					if(!empty($isma)){
						$status_isma = 1;
						echo "<br><strong>Status : </strong><span class='alert-success' style='padding:2px'><span class='glyphicon glyphicon-ok-sign'></span>&nbsp;<font color='#3c763d'>Anda sudah meng-upload file.</font>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='"; echo base_url('uploads/wisuda/'); echo "/"; echo $isma; echo "' target='_blank'>Preview&nbsp;</a></span>";
					}else{
						$status_isma = 0;
						echo "<br><strong>Status : </strong><span class='alert-danger' style='padding:2px'><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;<font color='#a94442'>Anda belum meng-upload file.</font></span>";
					}
				?>
            <?php set_validation_icon('tempat_lahir') ?>
				<br>
            </div>
			<?php if (form_error('isma')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('isma', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
            </td>
         </tr>
		 <tr>
		  <td style="width:15%;"></td>
		  <td>
		  <div class="col-sm-6">
			<input type="submit" value="Upload" class="btn btn-default">
		  </div>
		  </td>
         </tr>
		 </table>
		 </form>
		 
		 
     <form action="<?=base_url()?>dashboard/wisuda/upload_pf" method="post" enctype="multipart/form-data">
	   <table class="table table-striped">
	   		
		 <tr>
			<h5 class="bg-success">C. File Pas Foto (Hitam-Putih)</h5>
         </tr>
		 
		 <tr>
          <td style="width:15%;"></td>
          <td>
            <div class="col-sm-6">
                <input type="file" name="file_pf" accept="image/gif, image/jpeg, image/png, image/bmp, application/pdf">
				<?php 
					if(!empty($pf)){
						$status_pf = 1;
						echo "<br><strong>Status : </strong><span class='alert-success' style='padding:2px'><span class='glyphicon glyphicon-ok-sign'></span>&nbsp;<font color='#3c763d'>Anda sudah meng-upload file.</font>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='"; echo base_url('uploads/wisuda/'); echo "/"; echo $pf; echo "' target='_blank'>Preview&nbsp;</a></span>";
					}else{
						$status_pf = 0;
						echo "<br><strong>Status : </strong><span class='alert-danger' style='padding:2px'><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;<font color='#a94442'>Anda belum meng-upload file.</font></span>";
					}
				?>
            <?php set_validation_icon('tempat_lahir') ?>
				<br>
            </div>
			<?php if (form_error('fp')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('fp', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
            </td>
         </tr>
		 <tr>
		  <td style="width:15%;"></td>
		  <td>
		  <div class="col-sm-6">
			<input type="submit" value="Upload" class="btn btn-default">
		  </div>
		  </td>
         </tr>
		 </table>
		 </form>
		 <hr>
		 <br>
		 <form method="POST" action="wisuda/update_alumni"> 
		 <input type="submit" value="Selesai" class="btn btn-primary" onclick="alumni"
		 <?php if ($status_btp == 0 || $status_isma == 0 || $status_pf == 0){ echo "disabled";}?>
		 />
		 </form>
     </form>
    </div>
   </div>    <!-- /panel -->
 </div> <!-- /container -->