<head>
    <title><?=$titel?></title> <!-- variabel diambil dari controller -->
     
    <link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet"> <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>assets/css/style.css" rel="stylesheet"> <!-- Custom styles for this template -->
</head>
 
<div class="container">
      <!-- Main component for a primary marketing message or call to action -->
  <?=$this->session->flashdata('pesan')?>
<div class="panel panel-default">
  <div class="panel-heading"><b>Form Upload Berkas Pra-yudisium</b></div>
  <div class="panel-body">
  
     <form action="<?=base_url()?>dashboard/upload/update_bt" method="post" enctype="multipart/form-data">
       <table class="table table-striped">
	   
		 <tr>
			<h4 class="bg-success">A. File Bebas Tanggungan</h4>
         </tr>
		 
		 <tr>
          <td style="width:15%;"></td>
          <td>
            <div class="col-sm-6">
                <input type="file" name="file_bt" accept="image/gif, image/jpeg, image/png, image/bmp, application/pdf">
				<?php 
					if(!empty($bt)){
						echo "<br><strong>Status : </strong><span class='alert-success' style='padding:2px'><span class='glyphicon glyphicon-ok-sign'></span>&nbsp;<font color='#3c763d'>Anda sudah meng-upload file.</font>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='"; echo base_url('uploads/prayudisium/'); echo "/"; echo $bt; echo "' target='_blank'>Preview&nbsp;</a></span>";
					}else{
						echo "<br><strong>Status : </strong><span class='alert-danger' style='padding:2px'><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;<font color='#a94442'>Anda belum meng-upload file.</font></span>";
					}
				?>
            </div>
          </td>
		  </tr>
		  <tr>
		  <td style="width:15%;"></td>
		  <td>
		  <div class="col-sm-6">
			<input type="submit" value="Upload" class="btn btn-default">
		  </div>
		  </td>
		 </tr>
		</table>
		</form>
		
		
     <form action="<?=base_url()?>dashboard/upload/update_ukt" method="post" enctype="multipart/form-data">
	   <table class="table table-striped">
	   		
		 <tr>
			<h4 class="bg-success">B. File Pembayaran UKT Terakhir</h4>
         </tr>
		 
		 <tr>
          <td style="width:15%;"></td>
          <td>
            <div class="col-sm-6">
                <input type="file" name="file_ukt" accept="image/gif, image/jpeg, image/png, image/bmp, application/pdf">
				<?php 
					if(!empty($ukt)){
						echo "<br><strong>Status : </strong><span class='alert-success' style='padding:2px'><span class='glyphicon glyphicon-ok-sign'></span>&nbsp;<font color='#3c763d'>Anda sudah meng-upload file.</font>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='"; echo base_url('uploads/prayudisium/'); echo "/"; echo $ukt; echo "' target='_blank'>Preview&nbsp;</a></span>";
					}else{
						echo "<br><strong>Status : </strong><span class='alert-danger' style='padding:2px'><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;<font color='#a94442'>Anda belum meng-upload file.</font></span>";
					}
				?>
            </div>
            </td>
         </tr>
		 <tr>
		  <td style="width:15%;"></td>
		  <td>
		  <div class="col-sm-6">
			<input type="submit" value="Upload" class="btn btn-default">
		  </div>
		  </td>
         </tr>
		 </table>
		 </form>
		 
		 
     <form action="<?=base_url()?>dashboard/upload/update_bta" method="post" enctype="multipart/form-data">
	   <table class="table table-striped">
	   		
		 <tr>
			<h4 class="bg-success">C. File Berita Acara Tugas Akhir</h4>
         </tr>
		 
		 <tr>
          <td style="width:15%;"></td>
          <td>
            <div class="col-sm-6">
                <input type="file" name="file_bta" accept="image/gif, image/jpeg, image/png, image/bmp, application/pdf">
				<?php 
					if(!empty($bta)){
						echo "<br><strong>Status : </strong><span class='alert-success' style='padding:2px'><span class='glyphicon glyphicon-ok-sign'></span>&nbsp;<font color='#3c763d'>Anda sudah meng-upload file.</font>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='"; echo base_url('uploads/prayudisium/'); echo "/"; echo $bta; echo "' target='_blank'>Preview&nbsp;</a></span>";
					}else{
						echo "<br><strong>Status : </strong><span class='alert-danger' style='padding:2px'><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;<font color='#a94442'>Anda belum meng-upload file.</font></span>";
					}
				?>
            </div>
            </td>
         </tr>
		 <tr>
		  <td style="width:15%;"></td>
		  <td>
		  <div class="col-sm-6">
			<input type="submit" value="Upload" class="btn btn-default">
		  </div>
		  </td>
         </tr>
		 </table>
		 </form>

     <form action="<?=base_url()?>dashboard/upload/update_bpkl" method="post" enctype="multipart/form-data">		 
	   <table class="table table-striped">
	   		
		 <tr>
			<h4 class="bg-success">D. File Berita Acara PKL</h4>
         </tr>
		 
		 <tr>
          <td style="width:15%;"></td>
          <td>
            <div class="col-sm-6">
                <input type="file" name="file_bpkl" accept="image/gif, image/jpeg, image/png, image/bmp, application/pdf">
				<?php 
					if(!empty($bpkl)){
						echo "<br><strong>Status : </strong><span class='alert-success' style='padding:2px'><span class='glyphicon glyphicon-ok-sign'></span>&nbsp;<font color='#3c763d'>Anda sudah meng-upload file.</font>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='"; echo base_url('uploads/prayudisium/'); echo "/"; echo $bpkl; echo "' target='_blank'>Preview&nbsp;</a></span>";
					}else{
						echo "<br><strong>Status : </strong><span class='alert-danger' style='padding:2px'><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;<font color='#a94442'>Anda belum meng-upload file.</font></span>";
					}
				?>
            </div>
            </td>
         </tr>
		 <tr>
		  <td style="width:15%;"></td>
		  <td>
		  <div class="col-sm-6">
			<input type="submit" value="Upload" class="btn btn-default">
		  </div>
		  </td>
         </tr>
		 </table>
		 </form>
		 
     <form action="<?=base_url()?>dashboard/upload/update_lta" method="post" enctype="multipart/form-data">
	   <table class="table table-striped">
	   		
		 <tr>
			<h4 class="bg-success">E. File Bukti Penyerahan Laporan Tugas Akhir</h4>
         </tr>
		 
		 <tr>
          <td style="width:15%;"></td>
          <td>
            <div class="col-sm-6">
                <input type="file" name="file_lta" accept="image/gif, image/jpeg, image/png, image/bmp, application/pdf">
				<?php 
					if(!empty($lta)){
						echo "<br><strong>Status : </strong><span class='alert-success' style='padding:2px'><span class='glyphicon glyphicon-ok-sign'></span>&nbsp;<font color='#3c763d'>Anda sudah meng-upload file.</font>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='"; echo base_url('uploads/prayudisium/'); echo "/"; echo $lta; echo "' target='_blank'>Preview&nbsp;</a></span>";
					}else{
						echo "<br><strong>Status : </strong><span class='alert-danger' style='padding:2px'><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;<font color='#a94442'>Anda belum meng-upload file.</font></span>";
					}
				?>
            </div>
            </td>
         </tr>
		 <tr>
		  <td style="width:15%;"></td>
		  <td>
		  <div class="col-sm-6">
			<input type="submit" value="Upload" class="btn btn-default">
		  </div>
		  </td>
         </tr>
		 </table>
		 </form>
		 
     <form action="<?=base_url()?>dashboard/upload/update_lpkl" method="post" enctype="multipart/form-data">
	   <table class="table table-striped">
	   		
		 <tr>
			<h4 class="bg-success">F. File Bukti Penyerahan Laporan PKL</h4>
         </tr>
		 
		 <tr>
          <td style="width:15%;"></td>
          <td>
            <div class="col-sm-6">
                <input type="file" name="file_lpkl" accept="image/gif, image/jpeg, image/png, image/bmp, application/pdf">
				<?php 
					if(!empty($lpkl)){
						echo "<br><strong>Status : </strong><span class='alert-success' style='padding:2px'><span class='glyphicon glyphicon-ok-sign'></span>&nbsp;<font color='#3c763d'>Anda sudah meng-upload file.</font>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='"; echo base_url('uploads/prayudisium/'); echo "/"; echo $lpkl; echo "' target='_blank'>Preview&nbsp;</a></span>";
					}else{
						echo "<br><strong>Status : </strong><span class='alert-danger' style='padding:2px'><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;<font color='#a94442'>Anda belum meng-upload file.</font></span>";
					}
				?>
            </div>
          </td>
         </tr>
		 <tr>
		  <td style="width:15%;"></td>
		  <td>
		  <div class="col-sm-6">
			<input type="submit" value="Upload" class="btn btn-default">
		  </div>
		  </td>
         </tr>
		 </table>
		 </form>
		 
     <form action="<?=base_url()?>dashboard/upload/update_ns" method="post" enctype="multipart/form-data">
	   <table class="table table-striped">
	   		
		 <tr>
			<h4 class="bg-success">G. File Nilai Sementara</h4>
         </tr>
		 
		 <tr>
          <td style="width:15%;"></td>
          <td>
            <div class="col-sm-6">
                <input type="file" name="file_ns" accept="image/gif, image/jpeg, image/png, image/bmp, application/pdf">
				<?php 
					if(!empty($ns)){
						echo "<br><strong>Status : </strong><span class='alert-success' style='padding:2px'><span class='glyphicon glyphicon-ok-sign'></span>&nbsp;<font color='#3c763d'>Anda sudah meng-upload file.</font>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='"; echo base_url('uploads/prayudisium/'); echo "/"; echo $ns; echo "' target='_blank'>Preview&nbsp;</a></span>";
					}else{
						echo "<br><strong>Status : </strong><span class='alert-danger' style='padding:2px'><span class='glyphicon glyphicon-exclamation-sign'></span>&nbsp;<font color='#a94442'>Anda belum meng-upload file.</font></span>";
					}
				?>
            </div>
            </td>
         </tr>
		 <tr>
		  <td style="width:15%;"></td>
		  <td>
		  <div class="col-sm-6">
			<input type="submit" value="Upload" class="btn btn-default">
		  </div>
		  </td>
         </tr>
		 </table>
		 </form>
		 
		 <hr>
		 <form action="<?=base_url()?>dashboard/prayudisium">
		 <div  class="form-group">
			<div class="col-sm-5 col-sm-offset-3">
				<input type="submit" class="btn btn-primary" value="Kembali">
            <!--button type="reset" class="btn btn-default">Batal</button-->
			</div>
	   </div>
	   </form>
     </form>
    </div>
   </div>    <!-- /panel -->
 </div> <!-- /container -->