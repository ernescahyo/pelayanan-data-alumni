<?php
$login_status = $this->session->userdata('login_status');
$user_level = $this->session->userdata('user_level');
?>

<nav class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> 
					 <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
					 <img src="<?php echo base_url('asset/img/home.svg'); ?>" width="38px" style="margin-top:10px;margin-bottom:10px;margin-left:10px;margin-right:10px;">
				</div>
				

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
					
					 <!-- Link Peserta -->
            <?php echo (isset($halaman) && $halaman == 'home') ? '<li class="active">' : '<li>'; ?> <?php echo anchor(site_url('dashboard'), 'Beranda');?></li>
<!-- Dropdown Pendaftaran -->
            <?php echo (isset($halaman) && preg_match('#(prayudisium|wisuda|alumni|bekerja|studiljt|usaha|upload)#', $halaman)) ? '<li class="active">' : '<li>'; ?>
            <?php echo anchor('dashboard/#', '<span class="glyphicon glyphicon-file"></span> Pendaftaran<span class="caret"></span>', 'class="dropdown-toggle" data-toggle="dropdown"');?>
            <ul class="dropdown-menu multi-level" role="menu">
                <?php echo (isset($halaman) && preg_match('#(prayudisium|upload)#', $halaman)) ? '<li class="active">' : '<li>'; ?> <?php echo anchor('dashboard/prayudisium', '<span class="glyphicon glyphicon-file"></span> Prayudisium');?></li>
                <?php echo (isset($halaman) && $halaman == 'wisuda') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('dashboard/wisuda', '<span class="glyphicon glyphicon-file"></span> Wisuda');?></li>
                <?php echo (isset($halaman) && $halaman == 'alumni') ? '<li class="active">' : '<li class="dropdown-submenu">'; ?> <?php echo anchor('dashboard/#', '<span class="glyphicon glyphicon-file"></span> Alumni');?>
				<ul class="dropdown-menu" role="menu">
                    <?php echo (isset($halaman) && $halaman == 'bekerja') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('dashboard/bekerja', '<span class="glyphicon glyphicon-list-alt"></span> Form Bekerja');?></li>
					<?php echo (isset($halaman) && $halaman == 'studiljt') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('dashboard/studiljt', '<span class="glyphicon glyphicon-list-alt"></span> Form Studi Lanjut');?></li>
					<?php echo (isset($halaman) && $halaman == 'usaha') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('dashboard/usaha', '<span class="glyphicon glyphicon-list-alt"></span> Form Wirausaha');?></li>
				</ul>
				</li>
            </ul>
            </li>
            <!-- Dropdown Pendaftaran end -->
			
			<!-- Link Tracer Alumni -->
            <?php echo (isset($halaman) && $halaman == 'Alumni') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('dashboard/alumni', '<span class="glyphicon glyphicon-list-alt"></span> Lihat Alumni');?></li>
			
            <!-- Dropdown Informasi -->
            <?php echo (isset($halaman) && preg_match('#(pengumuman|prosedur|jadwal)#', $halaman)) ? '<li class="active">' : '<li>'; ?>
            <?php echo anchor('dashboard/#', '<span class="glyphicon glyphicon-info-sign"></span> Informasi<span class="caret"></span>', 'class="dropdown-toggle" data-toggle="dropdown"');?>
            <ul class="dropdown-menu" role="menu">
                <?php echo (isset($halaman) && $halaman == 'pengumuman') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('dashboard/pengumuman', '<span class="glyphicon glyphicon-bullhorn"></span> Pengumuman');?></li>
                </ul>
            </li>
            <!-- Dropdown Informasi end -->

            <!-- Link Kontak -->
            <?php echo (isset($halaman) && $halaman == 'kontak') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('dashboard/kontak', '<span class="glyphicon glyphicon-envelope"></span> Kontak'); ?></li>

            <!-- Link Dropdown Akun Saya -->
            <?php echo (isset($halaman) && preg_match('#(biodata|cetak-biodata)#', $halaman)) ? '<li class="active">' : '<li>'; ?>
            <?php echo anchor('dashboard/#', '<span class="glyphicon glyphicon-user"></span> Profil Saya<span class="caret"></span>', 'class="dropdown-toggle" data-toggle="dropdown"');?>
                <ul class="dropdown-menu" role="menu">
					<?php echo (isset($halaman) && $halaman == 'biodata') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('dashboard/biodata', '<span class="glyphicon glyphicon-pencil"></span> Atur Biodata');?></li>
                </ul>
            </li>
            <!-- Link Dropdown Akun Saya -->
					
	  	
		</ul>
		<ul class="nav navbar-nav navbar-right">
		<li>
		<?php if (($login_status == true) && ($user_level == 'peserta')) : ?>
        <?php echo anchor('dashboard/logout',' Logout',array('class' => 'navbar-link', 'data-confirm' => 'Anda yakin akan logout?')); ?>  
        <?php endif ?>
		</li>				
		</ul>
    </div> 
</nav>