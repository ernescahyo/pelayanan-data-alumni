<div class="container">
<h3>Form Prayudisum</h3>	
<hr class="hr-primary">  

     <?php echo form_open($form_action, array('id'=>'myform', 'class'=>'form-horizontal', 'role'=>'form')) ?>

    <!-- hidden field -->
    <?php echo form_hidden('id', $values->id);?>
    <?php echo form_hidden('nim', $values->nim);?>

 
<h5 class="bg-success">A. Data Mahasiswa</h5>
    <!-- nim -->
    <div class="form-group form-group-sm">        
        <?php echo form_label('NIM', 'NIM', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-2">
            <p class="form-control-static"><?php echo $values->nim;?></p>
        </div>
    </div>

    <!-- nama -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('nama')?>">        
        <?php echo form_label('Nama', 'nama', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-4">
            <?php echo form_input('nama', $values->nama, 'id="nama" class="form-control" placeholder="Nama" maxlength="64"') ?>
            <?php set_validation_icon('nama') ?>
        </div>
        <?php if (form_error('nama')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('nama', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	<!-- angkatan -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('angkatan')?>">
        <?php echo form_label('Angkatan', 'angkatan', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('angkatan', $values->angkatan, 'id="angkatan" class="form-control" placeholder="Angkatan" maxlength="4"') ?> 
            <?php set_validation_icon('angkatan') ?>
        </div>
        <?php if (form_error('angkatan')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('angkatan', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>	
	
	<h5 class="bg-success">B. Nilai</h5>
	
     <div class="form-group form-group-sm has-feedback <?php set_validation_style('ip_1')?>">
        <?php echo form_label('Semester 1', 'ip_1', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('ip_1', $values->ip_1, 'id="ip_1" class="form-control" placeholder="Masukkan Nilai" maxlength="4"') ?> 
            <?php set_validation_icon('ip_1') ?>
        </div>
        <?php if (form_error('ip_1')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('ip_1', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>	
    
	   <div class="form-group form-group-sm has-feedback <?php set_validation_style('ip_2')?>">
        <?php echo form_label('Semester 2', 'ip_2', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('ip_2', $values->ip_2, 'id="ip_2" class="form-control" placeholder="Masukkan Nilai" maxlength="4"') ?> 
            <?php set_validation_icon('ip_2') ?>
        </div>
        <?php if (form_error('ip_2')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('ip_2', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>	
	
		   <div class="form-group form-group-sm has-feedback <?php set_validation_style('ip_3')?>">
        <?php echo form_label('Semester 3', 'ip_3', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('ip_3', $values->ip_3, 'id="ip_3" class="form-control" placeholder="Masukkan Nilai" maxlength="4"') ?> 
            <?php set_validation_icon('ip_3') ?>
        </div>
        <?php if (form_error('ip_3')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('ip_3', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>	

	<div class="form-group form-group-sm has-feedback <?php set_validation_style('ip_4')?>">
        <?php echo form_label('Semester 4', 'ip_4', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('ip_4', $values->ip_4, 'id="ip_4" class="form-control" placeholder="Masukkan Nilai" maxlength="4"') ?> 
            <?php set_validation_icon('ip_4') ?>
        </div>
        <?php if (form_error('ip_4')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('ip_4', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>

	<div class="form-group form-group-sm has-feedback <?php set_validation_style('ip_5')?>">
        <?php echo form_label('Semester 5', 'ip_5', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('ip_5', $values->ip_5, 'id="ip_4" class="form-control" placeholder="Masukkan Nilai" maxlength="4"') ?> 
            <?php set_validation_icon('ip_5') ?>
        </div>
        <?php if (form_error('ip_5')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('ip_5', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	
	<div class="form-group form-group-sm has-feedback <?php set_validation_style('ip_6')?>">
        <?php echo form_label('Semester 6', 'ip_6', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('ip_6', $values->ip_6, 'id="ip_6" class="form-control" placeholder="Masukkan Nilai" maxlength="4"') ?> 
            <?php set_validation_icon('ip_6') ?>
        </div>
        <?php if (form_error('ip_6')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('ip_6', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>

		<div class="form-group form-group-sm has-feedback <?php set_validation_style('nilai_TA')?>">
        <?php echo form_label('Nilai TA', 'nilai_TA', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('nilai_TA', $values->nilai_TA, 'id="nilai_TA" class="form-control" placeholder="Masukkan Nilai" maxlength="4"') ?> 
            <?php set_validation_icon('nilai_TA') ?>
        </div>
        <?php if (form_error('nilai_TA')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('nilai_TA', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	
		<div class="form-group form-group-sm has-feedback <?php set_validation_style('judul_PKL')?>">
        <?php echo form_label('Judul PKL', 'judul_PKL', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-4">
        <?php echo form_input('judul_PKL', $values->judul_PKL, 'id="judul_PKL" class="form-control" placeholder="Masukkan Judul PKL" maxlength="4"') ?> 
            <?php set_validation_icon('judul_PKL') ?>
        </div>
        <?php if (form_error('judul_PKL')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('judul_PKL', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	
		<div class="form-group form-group-sm has-feedback <?php set_validation_style('nilai_PKL')?>">
        <?php echo form_label('Nilai PKL', 'nilai_PKL', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('nilai_PKL', $values->nilai_PKL, 'id="nilai_PKL" class="form-control" placeholder="Masukkan Nilai" maxlength="4"') ?> 
            <?php set_validation_icon('nilai_PKL') ?>
        </div>
        <?php if (form_error('nilai_PKL')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('nilai_PKL', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	
    <hr>
    <div class="form-group notif" style="z-index: 99999">
        <div class="col-sm-5 col-sm-offset-3">
            <?php echo form_button(array('content'=>'Simpan dan lanjutkan', 'type'=>'submit', 'class'=>'btn btn-primary', 'data-confirm'=>'Anda yakin akan menyimpan data ini?')) ?>
        </div>
    </div>

     <?php echo form_close() ?>
</div>