<HTML>
<title>Form Kerja</title>
<div class="container">
<h3>Form Pekerjaan</h3>
<hr class="hr-primary"> 
<?php echo form_open($form_action, array('id'=>'myform', 'class'=>'form-horizontal', 'role'=>'form')) ?>

    <!-- hidden field -->
    <?php echo form_hidden('id', $values->id);?>
    <?php echo form_hidden('nim', $values->nim);?>

 
<h5 class="bg-success">A. Data Perusahaan</h5>

    <!-- nim -->
    <div class="form-group form-group-sm">        
        <?php echo form_label('NIM', 'NIM', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-2">
            <p class="form-control-static"><?php echo $values->nim;?></p>
        </div>
    </div>
	<!-- nama_perusahaan -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('nama_perusahaan')?>">
        <?php echo form_label('Nama Perusahaan', 'nama_perusahaan', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('nama_perusahaan', $values->nama_perusahaan, 'id="nama_perusahaan" class="form-control" placeholder="Cantumkan Nama Perusahaan" maxlength="64"') ?> 
            <?php set_validation_icon('nama_perusahaan') ?>
        </div>
        <?php if (form_error('nama_perusahaan')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('nama_perusahaan', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
<!-- Website Perusahaan -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('website_perusahaan')?>">
        <?php echo form_label('Website Perusahaan', 'website_perusahaan', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('website_perusahaan', $values->website_perusahaan, 'id="website_perusahaan" class="form-control" placeholder="Cantumkan url Website Perusahaan" maxlength="64"') ?> 
            <?php set_validation_icon('website_perusahaan') ?>
        </div>
        <?php if (form_error('website_perusahaan')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('website_perusahaan', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>	
<h5 class="bg-success">B. Data Tempat Kerja</h5>
<!-- Instansi Tempat kerja -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('instansi_tempat_kerja')?>">
        <?php echo form_label('Instansi Tempat Bekerja', 'instansi tempat bekerja', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('instansi_tempat_kerja', $values->instansi_tempat_kerja, 'id="instansi_tempat_kerja" class="form-control" placeholder="Cantumkan Tempat Kerja Anda" maxlength="64"') ?> 
            <?php set_validation_icon('instansi_tempat_kerja') ?>
        </div>
        <?php if (form_error('instansi_tempat_kerja')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('instansi_tempat_kerja', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	
	<!-- tahun_masuk_kerja -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('tahun_masuk_kerja')?>">
        <?php echo form_label('Tahun Masuk Bekerja', 'Tahun Masuk Kerja', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('tahun_masuk_kerja', $values->tahun_masuk_kerja, 'id="tahun_masuk_kerja" class="form-control" placeholder="Cantumkan Tahun Masuk Kerja" maxlength="4"') ?> 
            <?php set_validation_icon('tahun_masuk_kerja') ?>
        </div>
        <?php if (form_error('tahun_masuk_kerja')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('tahun_masuk_kerja', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	<!-- posisi_jabatan_saat_ini -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('posisi_jabatan_saat_ini')?>">
        <?php echo form_label('Posisi Jabatan Saat ini', 'Posisi Jabatan Saat ini', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('posisi_jabatan_saat_ini', $values->posisi_jabatan_saat_ini, 'id="posisi_jabatan_saat_ini" class="form-control" placeholder="Cantumkan Posisi Jabatan saat ini" maxlength="64"') ?> 
            <?php set_validation_icon('posisi_jabatan_saat_ini') ?>
        </div>
        <?php if (form_error('posisi_jabatan_saat_ini')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('posisi_jabatan_saat_ini', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>

    <input type="checkbox" name="" value="Accept TOS" /> Anda mengisi form ini dengan keadaan SADAR dan BERTANGGUNGJAWAB<br><br>
<div class="form-group">
        <div class="col-sm-5 col-sm-offset-3">
            <?php echo form_button(array('content'=>'Simpan', 'type'=>'submit', 'class'=>'btn btn-primary', 'data-confirm'=>'Anda yakin akan menyimpan data ini?')) ?>
        </div>
    </div>
	<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
        Collapsible Group 1</a>
      </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse in">
      <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
      minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
      commodo consequat.</div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
        Collapsible Group 2</a>
      </h4>
    </div>
    <div id="collapse2" class="panel-collapse collapse">
      <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
      minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
      commodo consequat.</div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
        Collapsible Group 3</a>
      </h4>
    </div>
    <div id="collapse3" class="panel-collapse collapse">
      <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
      minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
      commodo consequat.</div>
    </div>
  </div>
</div>
</div>
<HTML>


