<head>
    <title><?=$titel?></title> <!-- variabel diambil dari controller -->
     
    <link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet"> <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>assets/css/style.css" rel="stylesheet"> <!-- Custom styles for this template -->
</head>
 
<div class="container">
      <!-- Main component for a primary marketing message or call to action -->
  <?=$this->session->flashdata('pesan')?>
<div class="panel panel-default">
  <div class="panel-heading"><b>Form Upload Berkas Pra-yudisium</b></div>
  <div class="panel-body">
  
     <form action="<?=base_url()?>dashboard/upload/update_bt" method="post" enctype="multipart/form-data">
       <table class="table table-striped">
	   
		 <tr>
			<h4 class="bg-success">A. File Bebas Tanggungan</h4>
         </tr>
		 
		 <tr>
          <td style="width:15%;"></td>
          <td>
            <div class="col-sm-6">
                <input type="file" name="file_bt" class="form-control" accept="image/gif, image/jpeg, image/png, image/bmp, application/pdf">
				<br>
            </div>
          </td>
		  </tr>
		  <tr>
		  <td style="width:15%;"></td>
		  <td>
		  <div class="col-sm-6">
			<input type="submit" value="Upload" class="btn btn-default">
		  </div>
		  </td>
		 </tr>
		</table>
		</form>
		
		
     <form action="<?=base_url()?>dashboard/upload/update_ukt" method="post" enctype="multipart/form-data">
	   <table class="table table-striped">
	   		
		 <tr>
			<h4 class="bg-success">B. File Pembayaran UKT Terakhir</h4>
         </tr>
		 
		 <tr>
          <td style="width:15%;"></td>
          <td>
            <div class="col-sm-6">
                <input type="file" name="file_ukt" class="form-control" accept="image/gif, image/jpeg, image/png, image/bmp, application/pdf">
				<br>
            </div>
            </td>
         </tr>
		 <tr>
		  <td style="width:15%;"></td>
		  <td>
		  <div class="col-sm-6">
			<input type="submit" value="Upload" class="btn btn-default">
		  </div>
		  </td>
         </tr>
		 </table>
		 </form>
		 
		 
     <form action="<?=base_url()?>dashboard/upload/update_bta" method="post" enctype="multipart/form-data">
	   <table class="table table-striped">
	   		
		 <tr>
			<h4 class="bg-success">C. File Berita Acara Tugas Akhir</h4>
         </tr>
		 
		 <tr>
          <td style="width:15%;"></td>
          <td>
            <div class="col-sm-6">
                <input type="file" name="file_bta" class="form-control" accept="image/gif, image/jpeg, image/png, image/bmp, application/pdf">
				<br>
            </div>
            </td>
         </tr>
		 <tr>
		  <td style="width:15%;"></td>
		  <td>
		  <div class="col-sm-6">
			<input type="submit" value="Upload" class="btn btn-default">
		  </div>
		  </td>
         </tr>
		 </table>
		 </form>

     <form action="<?=base_url()?>dashboard/upload/update_bpkl" method="post" enctype="multipart/form-data">		 
	   <table class="table table-striped">
	   		
		 <tr>
			<h4 class="bg-success">D. File Berita Acara PKL</h4>
         </tr>
		 
		 <tr>
          <td style="width:15%;"></td>
          <td>
            <div class="col-sm-6">
                <input type="file" name="file_bpkl" class="form-control" accept="image/gif, image/jpeg, image/png, image/bmp, application/pdf">
				<br>
            </div>
            </td>
         </tr>
		 <tr>
		  <td style="width:15%;"></td>
		  <td>
		  <div class="col-sm-6">
			<input type="submit" value="Upload" class="btn btn-default">
		  </div>
		  </td>
         </tr>
		 </table>
		 </form>
		 
     <form action="<?=base_url()?>dashboard/upload/update_lta" method="post" enctype="multipart/form-data">
	   <table class="table table-striped">
	   		
		 <tr>
			<h4 class="bg-success">E. File Bukti Penyerahan Laporan Tugas Akhir</h4>
         </tr>
		 
		 <tr>
          <td style="width:15%;"></td>
          <td>
            <div class="col-sm-6">
                <input type="file" name="file_lta" class="form-control" accept="image/gif, image/jpeg, image/png, image/bmp, application/pdf">
				<br>
            </div>
            </td>
         </tr>
		 <tr>
		  <td style="width:15%;"></td>
		  <td>
		  <div class="col-sm-6">
			<input type="submit" value="Upload" class="btn btn-default">
		  </div>
		  </td>
         </tr>
		 </table>
		 </form>
		 
     <form action="<?=base_url()?>dashboard/upload/update_lpkl" method="post" enctype="multipart/form-data">
	   <table class="table table-striped">
	   		
		 <tr>
			<h4 class="bg-success">F. File Bukti Penyerahan Laporan PKL</h4>
         </tr>
		 
		 <tr>
          <td style="width:15%;"></td>
          <td>
            <div class="col-sm-6">
                <input type="file" name="file_lpkl" class="form-control" accept="image/gif, image/jpeg, image/png, image/bmp, application/pdf">
				<br>
            </div>
          </td>
         </tr>
		 <tr>
		  <td style="width:15%;"></td>
		  <td>
		  <div class="col-sm-6">
			<input type="submit" value="Upload" class="btn btn-default">
		  </div>
		  </td>
         </tr>
		 </table>
		 </form>
		 
     <form action="<?=base_url()?>dashboard/upload/update_ns" method="post" enctype="multipart/form-data">
	   <table class="table table-striped">
	   		
		 <tr>
			<h4 class="bg-success">G. File Nilai Sementara</h4>
         </tr>
		 
		 <tr>
          <td style="width:15%;"></td>
          <td>
            <div class="col-sm-6">
                <input type="file" name="file_ns" class="form-control" accept="image/gif, image/jpeg, image/png, image/bmp, application/pdf">
				<br>
            </div>
            </td>
         </tr>
		 <tr>
		  <td style="width:15%;"></td>
		  <td>
		  <div class="col-sm-6">
			<input type="submit" value="Upload" class="btn btn-default">
		  </div>
		  </td>
         </tr>
		 </table>
		 </form>
		 
		 <hr>
		 <form action="<?=base_url()?>dashboard/prayudisium">
		 <div  class="form-group">
			<div class="col-sm-5 col-sm-offset-3">
				<input type="submit" class="btn btn-primary" value="Kembali">
            <!--button type="reset" class="btn btn-default">Batal</button-->
			</div>
	   </div>
	   </form>
     </form>
    </div>
   </div>    <!-- /panel -->
 </div> <!-- /container -->