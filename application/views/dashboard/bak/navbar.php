<?php
$login_status = $this->session->userdata('login_status');
$user_level = $this->session->userdata('user_level');
?>
<style>
  /* Note: Try to remove the following lines to see the effect of CSS positioning */
  .affix {
      top: 0;
      width: 100%;
	  z-index: 100;
  }
  
  .header {
	  background: #ffffff;
	  padding: 0px;
	  height: 100px;
  }
  
  

  .affix + .container-fluid {
	  position: relative;
      top: 0px;
  }
  </style>
  
<nav class="navbar navbar-inverse" role="navigation" data-spy="affix" data-offset-top="97">
    <div class="container">

        <!-- Navbar Link -->
        <ul class="nav navbar-nav navbar-left">
            <!-- Link Peserta -->
            <?php echo (isset($halaman) && $halaman == 'home') ? '<li class="active">' : '<li>'; ?> <?php echo anchor(site_url('dashboard'), '<span class="glyphicon glyphicon-home"></span> Home');?></li>

			
			<!-- Dropdown Pendaftaran -->
            <?php echo (isset($halaman) && preg_match('#(pengumuman|prosedur|jadwal)#', $halaman)) ? '<li class="active">' : '<li>'; ?>
            <?php echo anchor('#', '<span class="glyphicon glyphicon-file"></span> Pendaftaran<span class="caret"></span>', 'class="dropdown-toggle" data-toggle="dropdown"');?>
            <ul class="dropdown-menu" role="menu">
                <?php echo (isset($halaman) && $halaman == 'prayudisium') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('dashboard/prayudisium', '<span class="glyphicon glyphicon-file"></span> Prayudisium');?></li>
                <?php echo (isset($halaman) && $halaman == 'wisuda') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('dashboard/wisuda', '<span class="glyphicon glyphicon-file"></span> Wisuda');?></li>
                <?php echo (isset($halaman) && $halaman == 'alumni') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('#', '<span class="glyphicon glyphicon-file"></span> Alumni');?></li>
            </ul>
            </li>
            <!-- Dropdown Pendaftaran end -->
			
			<!-- Link Tracer Alumni -->
            <?php echo (isset($halaman) && $halaman == 'alumni') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('#', '<span class="glyphicon glyphicon-list-alt"></span> Lihat Alumni');?></li>
			
            <!-- Dropdown Informasi -->
            <?php echo (isset($halaman) && preg_match('#(pengumuman|prosedur|jadwal)#', $halaman)) ? '<li class="active">' : '<li>'; ?>
            <?php echo anchor('#', '<span class="glyphicon glyphicon-info-sign"></span> Informasi<span class="caret"></span>', 'class="dropdown-toggle" data-toggle="dropdown"');?>
            <ul class="dropdown-menu" role="menu">
                <?php echo (isset($halaman) && $halaman == 'pengumuman') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('dashboard/pengumuman', '<span class="glyphicon glyphicon-bullhorn"></span> Pengumuman');?></li>
                <?php echo (isset($halaman) && $halaman == 'prosedur') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('dashboard/prosedur', '<span class="glyphicon glyphicon-question-sign"></span> Prosedur');?></li>
                <?php echo (isset($halaman) && $halaman == 'jadwal') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('dashboard/jadwal', '<span class="glyphicon glyphicon-calendar"></span> ####');?></li>
            </ul>
            </li>
            <!-- Dropdown Informasi end -->

            <!-- Link Kontak -->
            <?php echo (isset($halaman) && $halaman == 'kontak') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('dashboard/kontak', '<span class="glyphicon glyphicon-envelope"></span> Kontak'); ?></li>

            <!-- Link Dropdown Akun Saya -->
            <?php echo (isset($halaman) && preg_match('#(biodata|cetak-biodata)#', $halaman)) ? '<li class="active">' : '<li>'; ?>
            <?php echo anchor('#', '<span class="glyphicon glyphicon-user"></span> Profil Saya<span class="caret"></span>', 'class="dropdown-toggle" data-toggle="dropdown"');?>
                <ul class="dropdown-menu" role="menu">
					<?php echo (isset($halaman) && $halaman == 'akun') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('#', '<span class="glyphicon glyphicon-pencil"></span> Atur Profil');?></li>
                    <?php echo (isset($halaman) && $halaman == 'biodata') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('dashboard/biodata', '<span class="glyphicon glyphicon-pencil"></span> Atur Biodata');?></li>
                    <?php echo (isset($halaman) && $halaman == 'cetak-biodata') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('dashboard/biodata-preview', '<span class="glyphicon glyphicon-print"></span> Cetak Biodata');?></li>
                </ul>
            </li>
            <!-- Link Dropdown Akun Saya -->
        </ul>
        <!-- Navbar Link end -->

        <!-- Informasi login -->
        <?php if (($login_status == true) && ($user_level == 'peserta')) : ?>
        <p class="navbar-text navbar-right">
            Login sebagai, <?php echo $this->session->userdata('nama_panggilan');?> &nbsp;&nbsp; | &nbsp;&nbsp;
            <strong>
					<?php echo anchor('dashboard/logout',
                     '<span class="glyphicon glyphicon-ban-circle"></span>' . ' Logout',
                    array('class' => 'navbar-link', 'data-confirm' => 'Anda yakin akan logout?')); ?>
            </strong>
        </p>
        <?php endif ?>
        <!-- end Informasi login -->

    </div> <!-- container -->
</nav>