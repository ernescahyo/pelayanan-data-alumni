  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
<div class="container">
<h2>Biodata <a href="prosedur"><img src="<?php echo base_url('asset/img/help.png');?>" width="20px" height="20px" data-toggle="tooltip" data-placement="right" title="Bantuan"></a></h2>

<hr>

<?php echo form_open($form_action, array('id'=>'myform', 'class'=>'form-horizontal', 'role'=>'form')) ?>

    <!-- hidden field -->
    <?php echo form_hidden('id', $values->id);?>
    <?php echo form_hidden('nim', $values->nim);?>

    <h3 class="bg-success">A. Data Pribadi Mahasiswa</h3>

    <!-- nim -->
    <div class="form-group form-group-sm">        
        <?php echo form_label('NIM', 'NIM', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-2">
            <p class="form-control-static"><?php echo $values->nim;?></p>
        </div>
    </div>

    <!-- nama -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('nama')?>">        
        <?php echo form_label('Nama', 'nama', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-4">
            <?php echo form_input('nama', $values->nama, 'id="nama" class="form-control" placeholder="Nama" maxlength="64"') ?>
            <?php set_validation_icon('nama') ?>
        </div>
        <?php if (form_error('nama')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('nama', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>

    <!-- jenis_kelamin -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('jenis_kelamin')?>">
        <?php echo form_label('Jenis Kelamin', 'jenis_kelamin', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-4">
            <label class="radio-inline" for="laki-laki">
                <?php echo form_radio('jenis_kelamin', 'L', (isset($values->jenis_kelamin) && $values->jenis_kelamin == 'L') ? true : false, 'id ="laki-laki"')?> Laki-laki
            </label>
            <label class="radio-inline" for="perempuan">
                <?php echo form_radio('jenis_kelamin', 'P', (isset($values->jenis_kelamin) && $values->jenis_kelamin == 'P') ? true : false, 'id ="perempuan"')?> Perempuan
            </label>
        </div>
        <?php if (form_error('jenis_kelamin')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('jenis_kelamin', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	
	<!-- tempat_lahir -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('tempat_lahir')?>">
        <?php echo form_label('Tempat Lahir', 'tempat_lahir', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
            <?php echo form_input('tempat_lahir', $values->tempat_lahir, 'id="tempat_lahir" class="form-control" placeholder="Tempat Lahir" maxlength="32"') ?>
            <?php set_validation_icon('tempat_lahir') ?>
        </div>
        <?php if (form_error('tempat_lahir')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('tempat_lahir', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>

    <!-- tanggal_lahir -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('tanggal_lahir')?>">        
        <?php echo form_label('Tanggal Lahir', 'tanggal_lahir', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-2">
            <div class="input-group date" data-date-format="dd-mm-yyyy">
                <?php echo form_input('tanggal_lahir', date_to_id($values->tanggal_lahir), 'id="tanggal_lahir" class="form-control" placeholder="Tanggal Lahir" maxlength="10"') ?>
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </div>
        <?php if (form_error('tanggal_lahir')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('tanggal_lahir', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	
	<!-- perkawinan -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('status_perkawinan')?>">
        <?php echo form_label('Status Perkawinan', 'status_perkawinan', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-4">
            <label class="radio-inline" for="sudah">
                <?php echo form_radio('status_perkawinan', 'S', (isset($values->status_perkawinan) && $values->status_perkawinan == 'S') ? true : false, 'id ="sudah"')?> Sudah</label>
            <label class="radio-inline" for="belum">
                <?php echo form_radio('status_perkawinan', 'B', (isset($values->status_perkawinan) && $values->status_perkawinan == 'B') ? true : false, 'id ="belum"')?> Belum</label>
        </div>
        <?php if (form_error('status_perkawinan')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('status_perkawinan', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	
	<!-- berat_badan -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('berat_badan')?>">        
        <?php echo form_label('Berat Badan', 'berat_badan', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-2">
            <div class="input-group">
                <?php echo form_input('berat_badan', $values->berat_badan, 'id="berat_badan" class="form-control" placeholder="Berat Badan" maxlength="3"') ?>
                <span class="input-group-addon">kg</span>
            </div>
        </div>
        <?php if (form_error('berat_badan')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('berat_badan', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>

    <!-- tinggi_badan -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('tinggi_badan')?>">        
        <?php echo form_label('Tinggi Badan', 'tinggi_badan', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-2">
            <div class="input-group">
                <?php echo form_input('tinggi_badan', $values->tinggi_badan, 'id="tinggi_badan" class="form-control" placeholder="Tinggi Badan" maxlength="3"') ?>
                <span class="input-group-addon">cm</span>
            </div>
        </div>
        <?php if (form_error('tinggi_badan')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('tinggi_badan', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	
	    <div class="form-group form-group-sm has-feedback <?php set_validation_style('agama')?>">        
        <?php echo form_label('Agama', 'agama', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-2">
            <?php
            $agama = array(
                '' => '- Pilih -',
                '1' => 'Islam',
                '2' => 'Katolik',
                '3' => 'Protestan',
                '4' => 'Hindu',
                '5' => 'Budha',
                '6' => 'Konghucu',
                '0' => 'Lainnya',
            );
            $atribut_agama = 'class="form-control"';
            echo form_dropdown('agama', $agama, $values->agama, $atribut_agama);
            ?>
        </div>
        <?php if (form_error('agama')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('agama', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	
	<!-- tmp_alamat -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('tmp_alamat')?>">
        <?php echo form_label('Alamat', 'tmp_alamat', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-5">
            <?php echo form_textarea('tmp_alamat', $values->tmp_alamat, 'class="form-control" id="tmp_alamat" placeholder="Alamat Tinggal"') ?>
        </div>
        <?php if (form_error('tmp_alamat')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('tmp_alamat', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>

    <!-- tmp_telepon -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('tmp_telepon')?>">        
        <?php echo form_label('Telepon', 'tmp_telepon', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
            <?php echo form_input('tmp_telepon', $values->tmp_telepon, 'id="tmp_telepon" class="form-control" placeholder="Telepon Tempat Tinggal" maxlength="16"') ?>
            <?php set_validation_icon('tmp_telepon') ?>
        </div>
        <?php if (form_error('tmp_telepon')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('tmp_telepon', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	
	<div class="form-group form-group-sm has-feedback <?php set_validation_style('ort_nama_ayah')?>">
        <?php echo form_label('Nama Ayah', 'ort_nama_ayah', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-4">
            <?php echo form_input('ort_nama_ayah', $values->ort_nama_ayah, 'id="ort_nama_ayah" class="form-control" placeholder="Nama Ayah" maxlength="64"') ?>
            <?php set_validation_icon('ort_nama_ayah') ?>
        </div>
        <?php if (form_error('ort_nama_ayah')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('ort_nama_ayah', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	
	 <h3 class="bg-success">B. Data Akademik Mahasiswa</h3>
	
	 <div class="form-group form-group-sm has-feedback">     
	<?php echo form_label('Fakultas', 'fakultas', array('class' => 'control-label col-sm-3')) ?>
	<div class="col-sm-3">
            <p class="form-control-static"><strong><?php echo 'Politeknik Negeri Jember';?></strong></p>
        </div>
	</div>
	
	
	 <div class="form-group form-group-sm has-feedback">     
	<?php echo form_label('Jurusan/Program Studi', 'jur_prodi', array('class' => 'control-label col-sm-3')) ?>
	<div class="col-sm-4">
            <p class="form-control-static"><strong><?php echo 'Teknologi Informasi, Manajemen Informatika';?></strong></p>
        </div>
	</div>
	
	<!-- tanggal_lulus -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('tanggal_lulus')?>">        
        <?php echo form_label('Tanggal Lulus', 'tanggal_lulus', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-2">
            <div class="input-group date" data-date-format="dd-mm-yyyy">
                <?php echo form_input('tanggal_lulus', date_to_id($values->tanggal_lulus), 'id="tanggal_lulus" class="form-control" placeholder="Tanggal Lulus" maxlength="10"') ?>
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </div>
        <?php if (form_error('tanggal_lulus')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('tanggal_lulus', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	

    <div class="form-group form-group-sm has-feedback <?php set_validation_style('lama_studi')?>">        
        <?php echo form_label('Lama Studi', 'lama_studi', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-2">
            <?php echo form_input('lama_studi', $values->lama_studi, 'id="lama_studi" class="form-control" placeholder="Lama Studi" maxlength="2"') ?>
            <?php set_validation_icon('lama_studi') ?>
        </div>
        <?php if (form_error('lama_studi')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('lama_studi', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>

	    <div class="form-group form-group-sm has-feedback <?php set_validation_style('anak_ke')?>">        
        <?php echo form_label('Indeks Prestasi Kumulatif', 'ipk', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-2">
            <?php echo form_input('ipk', $values->ipk, 'id="ipk" class="form-control" placeholder="IPK" maxlength="5"') ?>
            <?php set_validation_icon('ipk') ?>
        </div>
        <?php if (form_error('ipk')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('ipk', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
   
     <div class="form-group form-group-sm has-feedback <?php set_validation_style('lama_lap')?>">        
        <?php echo form_label('Lama Penyusunan Laporan', 'lama_lap', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-2">
            <?php echo form_input('lama_lap', $values->lama_lap, 'id="lama_lap" class="form-control" placeholder="Lama Penyusunan" maxlength="2"') ?>
            <?php set_validation_icon('lama_lap') ?>
        </div>
        <?php if (form_error('lama_lap')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('lama_lap', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	
	  <div class="form-group form-group-sm has-feedback <?php set_validation_style('judul_ta')?>">        
        <?php echo form_label('Judul Laporan TA', 'judul_ta', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-2">
            <?php echo form_input('judul_ta', $values->judul_ta, 'id="judul_ta" class="form-control" placeholder="Judul Tugas Akhir" maxlength="100"') ?>
            <?php set_validation_icon('judul_ta') ?>
        </div>
        <?php if (form_error('judul_ta')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('judul_ta', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>

	  <div class="form-group form-group-sm has-feedback <?php set_validation_style('dp1')?>">        
        <?php echo form_label('Pembimbing 1', 'dp1', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-2">
            <?php
            $dp1 = array(
                '' => '- Pilih -',
                '1' => 'Dwi Putro S',
                '2' => 'Hendra Yufit',
                '3' => 'Ganang Aji',
                
            );
            $atribut_dp1 = 'class="form-control"';
            echo form_dropdown('dp1', $dp1, $values->dp1, $atribut_dp1);
            ?>
        </div>
        <?php if (form_error('dp1')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('dp1', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	
	 <div class="form-group form-group-sm has-feedback <?php set_validation_style('dp2')?>">        
        <?php echo form_label('Pembimbing 2', 'dp2', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-2">
            <?php
            $dp2 = array(
                '' => '- Pilih -',
                '1' => 'Dwi Putro S',
                '2' => 'Hendra Yufit',
                '3' => 'Ganang Aji',
                
            );
            $atribut_dp2 = 'class="form-control"';
            echo form_dropdown('dp2', $dp2, $values->dp2, $atribut_dp2);
            ?>
        </div>
        <?php if (form_error('dp2')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('dp2', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	

    <hr>
    <div class="form-group">
        <div class="col-sm-5 col-sm-offset-3">
            <?php echo form_button(array('content'=>'Simpan', 'type'=>'submit', 'class'=>'btn btn-primary', 'data-confirm'=>'Anda yakin akan menyimpan data ini?')) ?>
        </div>
    </div>

<?php echo form_close() ?>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>

</div>