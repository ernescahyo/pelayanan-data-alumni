<HTML>
<title>Form Kerja</title>
<div class="container">
<h3>Form Wirausaha</h3>
<hr class="hr-primary"> 
<?php echo form_open($form_action, array('id'=>'myform', 'class'=>'form-horizontal', 'role'=>'form')) ?>

    <!-- hidden field -->
    <?php echo form_hidden('id', $values->id);?>
    <?php echo form_hidden('nim', $values->nim);?>

 
<h5 class="bg-success">Data Usaha</h5>

    <!-- nim -->
    <div class="form-group form-group-sm">        
        <?php echo form_label('NIM', 'NIM', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-2">
            <p class="form-control-static"><?php echo $values->nim;?></p>
        </div>
    </div>
	<!-- nama_usaha -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('nama_usaha')?>">
        <?php echo form_label('Nama Usaha', 'nama_usaha', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('nama_usaha', $values->nama_usaha, 'id="nama_usaha" class="form-control" placeholder="Cantumkan Nama Usaha" maxlength="64"') ?> 
            <?php set_validation_icon('nama_usaha') ?>
        </div>
        <?php if (form_error('nama_usaha')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('nama_usaha', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
<!-- Website Perusahaan -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('alamat_usaha')?>">
        <?php echo form_label('Alamat Usaha', 'alamat_usaha', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('alamat_usaha', $values->alamat_usaha, 'id="alamat_usaha" class="form-control" placeholder="Cantumkan Alamat" maxlength="64"') ?> 
            <?php set_validation_icon('alamat_usaha') ?>
        </div>
        <?php if (form_error('alamat_usaha')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('alamat_usaha', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>	

<!-- Instansi Tempat kerja -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('website_usaha')?>">
        <?php echo form_label('Website Usaha', 'website_usaha', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('website_usaha', $values->website_usaha, 'id="website_usaha" class="form-control" placeholder="Cantumkan website usaha (optional)" maxlength="64"') ?> 
            <?php set_validation_icon('website_usaha') ?>
        </div>
        <?php if (form_error('website_usaha')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('website_usaha', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	
	<!-- bidang_usaha -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('bidang_usaha')?>">
        <?php echo form_label('Bidang Usaha', 'Jenis Pendidikan', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('bidang_usaha', $values->bidang_usaha, 'id="bidang_usaha" class="form-control" placeholder="Cantumkan Bidang Usaha" maxlength="30"') ?> 
            <?php set_validation_icon('bidang_usaha') ?>
        </div>
        <?php if (form_error('bidang_usaha')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('bidang_usaha', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	<!-- posisi_jabatan_saat_ini -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('tahun_berdiri')?>">
        <?php echo form_label('Tahun Berdiri', 'Tahun Berdiri', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('tahun_berdiri', $values->tahun_berdiri, 'id="tahun_berdiri" class="form-control" placeholder="Cantumkan Tahun mulai" maxlength="64"') ?> 
            <?php set_validation_icon('tahun_berdiri') ?>
        </div>
        <?php if (form_error('tahun_berdiri')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('tahun_berdiri', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	

<div class="form-group">
        <div class="col-sm-5 col-sm-offset-3">
            <?php echo form_button(array('content'=>'Simpan', 'type'=>'submit', 'class'=>'btn btn-primary', 'data-confirm'=>'Anda yakin akan menyimpan data ini?')) ?>
        </div>
    </div>
	
</div>
<HTML>


