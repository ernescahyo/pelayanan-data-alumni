<HTML>
<title>Form Kerja</title>
<div class="container">
<h3>Form Studi Lanjut</h3>
<hr class="hr-primary"> 
<?php echo form_open($form_action, array('id'=>'myform', 'class'=>'form-horizontal', 'role'=>'form')) ?>

    <!-- hidden field -->
    <?php echo form_hidden('id', $values->id);?>
    <?php echo form_hidden('nim', $values->nim);?>

 
<h5 class="bg-success">Data Perguruan Tinggi</h5>

    <!-- nim -->
    <div class="form-group form-group-sm">        
        <?php echo form_label('NIM', 'NIM', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-2">
            <p class="form-control-static"><?php echo $values->nim;?></p>
        </div>
    </div>
	<!-- nama_perusahaan -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('nama_pt')?>">
        <?php echo form_label('Nama Perguruan Tinggi', 'nama_pt', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('nama_pt', $values->nama_pt, 'id="nama_pt" class="form-control" placeholder="Cantumkan Nama Perguruan Tinggi" maxlength="64"') ?> 
            <?php set_validation_icon('nama_pt') ?>
        </div>
        <?php if (form_error('nama_pt')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('nama_pt', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
<!-- Website Perusahaan -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('kota')?>">
        <?php echo form_label('Kota', 'kota', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('kota', $values->kota, 'id="kota" class="form-control" placeholder="Cantumkan Nama Kota" maxlength="64"') ?> 
            <?php set_validation_icon('kota') ?>
        </div>
        <?php if (form_error('kota')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('kota', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>	

<!-- Instansi Tempat kerja -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('negara')?>">
        <?php echo form_label('Negara', 'negara', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('negara', $values->negara, 'id="negara" class="form-control" placeholder="Cantumkan Tempat Kerja Anda" maxlength="64"') ?> 
            <?php set_validation_icon('negara') ?>
        </div>
        <?php if (form_error('negara')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('negara', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	
	<!-- j_pendidikan -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('j_pendidikan')?>">
        <?php echo form_label('Jenis Pendidikan', 'Jenis Pendidikan', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('j_pendidikan', $values->j_pendidikan, 'id="j_pendidikan" class="form-control" placeholder="Cantumkan Jenis Pendidikan" maxlength="30"') ?> 
            <?php set_validation_icon('j_pendidikan') ?>
        </div>
        <?php if (form_error('j_pendidikan')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('j_pendidikan', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	<!-- posisi_jabatan_saat_ini -->
    <div class="form-group form-group-sm has-feedback <?php set_validation_style('bidang_studi')?>">
        <?php echo form_label('Bidang Studi yang Ditempuh', 'Bidang Studi', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('bidang_studi', $values->bidang_studi, 'id="bidang_studi" class="form-control" placeholder="Cantumkan Posisi Jabatan saat ini" maxlength="64"') ?> 
            <?php set_validation_icon('bidang_studi') ?>
        </div>
        <?php if (form_error('bidang_studi')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('bidang_studi', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
	
	<div class="form-group form-group-sm has-feedback <?php set_validation_style('biaya')?>">
        <?php echo form_label('Sumber Biaya', 'Sumber Biaya', array('class' => 'control-label col-sm-3')) ?>
        <div class="col-sm-3">
        <?php echo form_input('biaya', $values->biaya, 'id="biaya" class="form-control" placeholder="Cantumkan Posisi Jabatan saat ini" maxlength="64"') ?> 
            <?php set_validation_icon('biaya') ?>
        </div>
        <?php if (form_error('biaya')) : ?>
            <div class="col-sm-9 col-sm-offset-3">
                <?php echo form_error('biaya', '<span class="help-block">', '</span>');?>
            </div>
        <?php endif ?>
    </div>
<div class="form-group">
        <div class="col-sm-5 col-sm-offset-3">
            <?php echo form_button(array('content'=>'Simpan', 'type'=>'submit', 'class'=>'btn btn-primary', 'data-confirm'=>'Anda yakin akan menyimpan data ini?')) ?>
        </div>
    </div>
	
</div>
<HTML>


