<?php
$login_status = $this->session->userdata('login_status');
$user_level = $this->session->userdata('user_level');
?>
<div class="container">
    <h5 class="bg-success">Data Alumni</h5>
   <!-- <img src="./gambar/<?=$row['upload_bebas_tanggungan'];?>" width="500" height="500">
    <img src="<?php //echo base_url('gambar/' $row->upload_bebas_tanggungan);?>">
    <?php echo base_url('gambar/');?><?php echo $row->upload_bebas_tanggungan;?>
    echo '<img src="/gambar/'.$row->upload_bebas_tanggungan'"/>';
    <img src="<?php echo base_url('/gambar/'); echo $row->upload_bebas_tanggungan;?>">-->
    <hr class="hr-primary">  
    <?php if (!empty($alumni) && is_array($alumni)): ?>
    <div class="row">
        <div class="col-md-12">
        <?php foreach ($alumni as $row) : ?>
		
            <article>

            <table border="1" width="75%" style="background:#f0f0f0;border-color: #ffffff">
<tr>        

<td style="width:17%; background:#F8F8F8;"  rowspan="5"> <img src="<?php echo base_url('/gambar/'); echo $row->upload_bebas_tanggungan;?>" width="150px" height="150px">
</td>

<td>&nbsp;&nbsp;<?php echo "Nama            :"?>&nbsp;&nbsp;<?php echo $row->nama ?></td>
</tr>
<tr>
<td>&nbsp;&nbsp;<?php echo "Alamat          :"?>&nbsp;&nbsp;<?php echo $row->tmp_alamat ?></td>
</tr>
<tr>
<td>&nbsp;&nbsp;<?php echo "No. Telepon     :"?>&nbsp;&nbsp;<?php echo $row->tmp_telepon ?></td>
</tr>
<tr>
<td>&nbsp;&nbsp;<?php echo "Status Bekerja  :"?>&nbsp;&nbsp;<?php echo $row->status_alumni ?></td>
</tr>
<tr>
<td>&nbsp;&nbsp;<?php echo "Status Bekerja  :"?>&nbsp;&nbsp;<?php echo $row->status_alumni ?></td>
</tr>
<br><br>
</table>
                
               
                <!-- Link edit dan hapus -->
                <?php
                if ($login_status == true && $user_level == 'administrator') {
                    echo '<br>';
                    echo anchor('admin/pengumuman/edit/'.$row->id, 'Edit') . ' | ';
                    echo anchor('admin/pengumuman/hapus/'.$row->id, 'Hapus', 'data-confirm="Anda yakin akan menghapus data ini?"');
                }
                ?>
                <!-- /Link edit dan hapus -->
            </article>
        <?php endforeach ?>
        </div>
    </div>
    <?php else: ?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <?php echo $pengumuman ?>
                </div>
            </div>
        </div>
    <?php endif ?>

    <hr>


    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?php echo (! empty($paging)) ? $paging : '' ?>
        </div>

        <div class="col-xs-12 col-md-6">
            <?php
            if ($user_level == 'administrator' && $login_status == true) {
                echo anchor('admin/pengumuman/tambah', 'Tambah', 'class="btn btn-primary" id="btn-tambah-pengumuman"');
            }
            ?>
        </div>
    </div>

</div> <!-- /Container-->