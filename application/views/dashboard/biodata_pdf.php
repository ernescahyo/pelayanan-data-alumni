<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Data Alumni</title>
    <style type="text/css">
        h1 {text-align:center; font-size:18px;}
        h2 {font-size:14px;}
        .tengah {text-align:center;	}
        .kiri {padding-left:10px;}
        table.nilai {border-collapse: collapse;}
        table.nilai td {border: 1px solid #000000}
    </style>
</head>

<body>
<h1>DATA ALUMNI</h1>
<hr />
<table width="500" border="0">
    <tr>
        <td colspan="2"><h2>A. DATA PRIBADI</h2></td>
    </tr>
    <tr>
        <td>NIM</td>
        <td>: <?php echo $peserta->nim ?></td>
    </tr>
    <tr>
        <td>Nama</td>
        <td>: <?php echo $peserta->nama ?></td>
    </tr>
    <tr>
        <td>Tempat, Tanggal Lahir </td>
        <td>:
            <?php echo $peserta->tempat_lahir ?>, <?php echo format_tanggal($peserta->tanggal_lahir) ?></td>
    </tr>
    <tr>
        <td>Jenis Kelamin </td>
        <td>:
            <?php echo format_jenis_kelamin($peserta->jenis_kelamin) ?></td>
    </tr>
    <tr>
        <td>Agama</td>
        <td>: <?php echo $peserta->agama != '0' ? format_agama($peserta->agama) : format_agama($peserta->ket_agama); ?></td>
    </tr>
    
    <tr>
        <td colspan="2"><h2>B. ORANG TUA</h2></td>
    </tr>
    <tr>
        <td>Nama Ayah </td>
        <td>: <?php echo $peserta->ort_nama_ayah ?></td>
    </tr>
  
    <tr>
        <td>Alamat</td>
        <td>: <?php echo $peserta->tmp_alamat ?></td>
    </tr>
    <tr>
        <td>Telepon</td>
        <td>: <?php echo $peserta->tmp_telepon ?></td>
    </tr>  
   
    <tr>
        <td colspan="2"><h2>D.  PENDIDIKAN </h2></td>
    </tr>
    <tr>
        <td>Perguruan Tiingg </td>
        <td>:  STMIK AUB Surakarta</td>
    </tr>
    <tr>
        <td>Prodi</td>
        <td>: <?php echo $peserta->jur_prodi ?></td>
    </tr>
    <tr>
        <td>Tanggal lulus</td>
        <td>: <?php echo $peserta->tanggal_lulus ?></td>
    </tr>
    <tr>
        <td>Lama Studi</td>
        <td>: <?php echo $peserta->lama_studi ?>&nbsp;bulan</td>
    </tr>
    <tr>
        <td>IPK </td>
        <td>: <?php echo $peserta->ipk ?></td>
    </tr>
    <tr>
        <td>Judul Skripsi/TA </td>
        <td>: <?php echo $peserta->judul_ta ?></td>
    </tr>
 
</table>
<p>&nbsp;</p>
<table width="600" border="0">
    <tr>
        <td width="200">
           
        </td>
        <td width="200">&nbsp;</td>
        <td width="200"><br />
            Surakarta, <?php $today = date("j F, Y"); ?><br>
            <br> 
            <br>
            <br>
            <br>
            <br>
            <?php echo $peserta->nama ?>
        </td>
    </tr>
</table>

</body>
</html>