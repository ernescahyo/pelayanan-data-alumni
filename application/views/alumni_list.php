<?php
$login_status = $this->session->userdata('login_status');
$user_level = $this->session->userdata('user_level');
?>
<div class="container">
    <h3>Daftar Alumni</h3>
    <hr class="hr-primary"> 
    <?php if (!empty($user) && is_array($user)): ?>
    <div class="row">
        <div class="col-md-12">
		            <article>
<div class="media">
        <?php 
		foreach ($user as $row):  ?>
<div class="bs-example">
<ul class="media-list">
<li class="media"><a class="pull-left" href="#"> <img class="media-object" style="width: 64px; height: 64px;" src="<?php echo base_url('/uploads/wisuda/'); echo "/"; echo $row->upload_pf;?>" /> </a>
<div class="media-body">
<h4 class="media-heading"><?php echo "Nama            :"?>&nbsp;&nbsp;<?php echo $row->nama ?></h4>
<p><?php echo "Alamat          :"?>&nbsp;&nbsp;<?php echo $row->tmp_alamat ?></p>
	<p><?php echo "No. Telepon     :"?>&nbsp;&nbsp;<?php echo $row->tmp_telepon ?></p>
	<p><?php echo "Status Bekerja  :"?>&nbsp;&nbsp;<?php echo $row->status_alumni ?></p>
	<p><?php echo "Status Bekerja  :"?>&nbsp;&nbsp;<?php echo $row->status_alumni ?></p>
</div>
</li>
</ul>
</div>
          
<?php endforeach ?>
</div>
                
               
                <!-- Link edit dan hapus -->
                <?php
                if ($login_status == true && $user_level == 'administrator') {
                    echo '<br>';
                    echo anchor('admin/pengumuman/edit/'.$row->id, 'Edit') . ' | ';
                    echo anchor('admin/pengumuman/hapus/'.$row->id, 'Hapus', 'data-confirm="Anda yakin akan menghapus data ini?"');
                }
                ?>
                <!-- /Link edit dan hapus -->
            </article>
        </div>
    </div>
    <?php else: ?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <?php echo $pengumuman ?>
                </div>
            </div>
        </div>
    <?php endif ?>

    <hr>


    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?php echo (! empty($paging)) ? $paging : '' ?>
        </div>

        <div class="col-xs-12 col-md-6">
            <?php
            if ($user_level == 'administrator' && $login_status == true) {
                echo anchor('admin/pengumuman/tambah', 'Tambah', 'class="btn btn-primary" id="btn-tambah-pengumuman"');
            }
            ?>
        </div>
    </div>

</div> <!-- /Container-->