<?php $user_level = $this->session->userdata('user_level')?>

 <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Menu</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Pelayanan Data Alumni</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
<?php echo (isset($halaman) && $halaman == 'home') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('admin', '<span class="glyphicon glyphicon-home"></span> Home');?></li>

            <?php if ($user_level == 'administrator') : ?>
                <?php echo (isset($halaman) && $halaman == 'user') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('admin/user', '<span class="glyphicon glyphicon-user"></span> User');?></li>
            <?php endif ?>

            <?php echo (isset($halaman) && $halaman == 'peserta') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('admin/peserta', '<span class="glyphicon glyphicon-list-alt"></span> Alumni');?></li>
            <?php if ($user_level == 'administrator') : ?>
                <?php echo (isset($halaman) && $halaman == 'pengumuman') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('admin/pengumuman', '<span class="glyphicon glyphicon-bullhorn"></span> Pengumuman'); ?></li>
            <?php endif ?>
            <?php echo (isset($halaman) && $halaman == 'kontak') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('admin/kontak', '<span class="glyphicon glyphicon-envelope"></span> Kontak'); ?></li>
            <?php echo (isset($halaman) && $halaman == 'myadmin') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('admin/myadmin', '<span class="glyphicon glyphicon-user"></span> MyAdmin'); ?></li>

          </ul>
		  
		  <p class="navbar-text navbar-right">
            Selamat datang <?php echo $this->session->userdata('username') ?>&nbsp;
            <strong>
                <?php echo anchor('admin/logout','Logout',
                    array('class' => 'navbar-link', 'data-confirm' => 'Anda yakin akan logout?')); ?>
            </strong>
        </p>

		  
        </div><!--/.nav-collapse -->