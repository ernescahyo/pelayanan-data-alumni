<nav class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> 
					 <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
					 <img src="<?php echo base_url('asset/img/home.svg'); ?>" width="38px" style="margin-top:10px;margin-bottom:10px;margin-left:10px;margin-right:10px;">
				</div>
				

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
       <?php echo (isset($halaman) && $halaman == 'home') ? '<li class="active">' : '<li>'; ?> <?php echo anchor(base_url(), 'Beranda');?></li>
	   <?php echo (isset($halaman) && $halaman == 'prosedur') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('prosedur', 'Prosedur');?></li>
	   <?php echo (isset($halaman) && $halaman == 'jadwal') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('jadwal', 'Jadwal');?></li>
	   <?php echo (isset($halaman) && $halaman == 'pendaftaran') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('pendaftaran', 'Daftar');?></li>	
		</ul>
		<ul class="nav navbar-nav navbar-right">
		<li><?php echo (isset($halaman) && $halaman == 'tanya jawab') ? '<li class="active">' : '<li>'; ?> <?php echo anchor('faq', 'FAQ');?></li>
		<li><?php echo anchor('login', 'Login'); ?></li>				
		</ul>
    </div> 
</nav>

