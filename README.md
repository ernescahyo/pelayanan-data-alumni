Issue kebutuhan data alumni di perguruan tinggi binaan menuju pencapaian perguruan tinggi berstandar BAN-PT dan kebutuhan data alumni bagi dunia industri saat ini sangat diperlukan. Untuk lebih mengoptimalkan fungsi tracer studi maupun data alumni di UPT kemahasiswaan, UPT BAAK, ketua program studi di perguruan tinggi sehingga dapat dimanfaatkan secara optimal. Sistem informasi pelayanan data alumni dilingkungan perguruan tinggi, sehingga dapat mudah diakses dari manapun dan dengan media apapun (PC, notebook, smartphone dan lainnya) menggunakan metode System Development Life Cycle (SDLC) dibangun dengan PHP Codeigniter dan basis data MySQL.

Lingkungan Kerja

1. Webserver Apache 2.x (http://www.apachefriends.org, XAMPP 1.7.x)
2. PHP 5.x (http://www.apachefriends.org, XAMPP 1.7.x)
3. MySQL 5.x (http://www.apachefriends.org, XAMPP 1.7.x)

Panduan Instalasi

1. Download Source menggunakan command line git clone
2. Database tersedia pada directory backup
3. Edit file database.php yang berada di dalam folder application/config/. Sesuaikan dengan setting komputer local anda.
  'hostname' => 'localhost',
  'username' => 'root',
  'password' => '',
  'database' => 'alumni5',
4. Edit file config.php yang berada di dalam folder application/config/. Sesuaikan $config['base_url']	= 'http://localhost/alumni5/';